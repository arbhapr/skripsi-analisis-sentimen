<?php
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login')->name('login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// third-party Twitter Auth
Route::get('twitter/auth', 'Twitter\authController@auth')->name('twitter-auth');
Route::get('twitter/callback', 'Twitter\authController@callback')->name('twitter-callback');

Route::redirect('/', '/dashboard');

Route::get('/dashboard', 'MainController@index')->middleware('auth')->name('dashboard');
Route::get('/settings', 'MainController@settings')->middleware('auth')->name('settings');
Route::post('/settings', 'MainController@updateSettings')->middleware('auth')->name('updateSettings');

// Skripsi Routes
Route::middleware(['auth'])->namespace('Skripsi')->name('skripsi.')->group( function() {
    Route::prefix('crawling')->name('crawling.')->group( function(){
        Route::get('/', 'CrawlingController@index')->name('index');
        Route::post('/', 'CrawlingController@crawling')->name('crawling');
    });

    Route::prefix('flagging')->name('flagging.')->group( function(){
        Route::get('/', 'FlaggingController@index')->name('index');
        Route::get('/training', 'FlaggingController@training')->name('training');
        Route::get('/count', 'FlaggingController@count')->name('count');
        Route::post('/', 'FlaggingController@flagging')->name('flagging');
        Route::get('/destroy', 'FlaggingController@destroy')->name('destroy');

    });

    Route::prefix('preprocessing')->name('preprocessing.')->group( function(){
        Route::get('/', 'PreprocessingController@index')->name('index');
    });

    Route::get('/a-dump', 'DumpController@index');
    Route::get('/b-dump', 'DumpController@naiveBayes');
    Route::get('/c-dump', 'DumpController@testing');
});
// Skripsi Routes