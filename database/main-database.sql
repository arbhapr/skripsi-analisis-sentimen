-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.5.0.5332
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table boilerplate.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table boilerplate.migrations: ~2 rows (approximately)
DELETE FROM `migrations`;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(11, '2019_08_19_091135_create_twitter_accounts_table', 1),
	(12, '2019_08_20_143318_create_tweets_table', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table boilerplate.tickets
CREATE TABLE IF NOT EXISTS `tickets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ticket_id` varchar(10) NOT NULL,
  `sender` varchar(255) NOT NULL,
  `uname_menfess` varchar(255) NOT NULL,
  `nominal` int(11) NOT NULL,
  `payment` enum('ovo','gopay','jenius','mandiri') NOT NULL,
  `receipt` varchar(255) DEFAULT NULL,
  `status` enum('pending','completed','rejected') NOT NULL DEFAULT 'pending',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table boilerplate.tickets: ~0 rows (approximately)
DELETE FROM `tickets`;
/*!40000 ALTER TABLE `tickets` DISABLE KEYS */;
INSERT INTO `tickets` (`id`, `ticket_id`, `sender`, `uname_menfess`, `nominal`, `payment`, `receipt`, `status`, `created_at`, `updated_at`) VALUES
	(1, '190825-624', '1085170784157425664', '@KampusMF', 150000, 'ovo', 'https://ton.twitter.com/1.1/ton/data/dm/1165600801437319172/1165600796680966144/A8X17pjd.jpg:small', 'pending', '2019-08-25 12:34:01', '2019-08-25 12:34:01');
/*!40000 ALTER TABLE `tickets` ENABLE KEYS */;

-- Dumping structure for table boilerplate.tweets
CREATE TABLE IF NOT EXISTS `tweets` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `dm_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'for deletion purpose',
  `twitter_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'TwitterAccount ID',
  `tweet` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tweet_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'TweetID',
  `sender` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table boilerplate.tweets: ~0 rows (approximately)
DELETE FROM `tweets`;
/*!40000 ALTER TABLE `tweets` DISABLE KEYS */;
/*!40000 ALTER TABLE `tweets` ENABLE KEYS */;

-- Dumping structure for table boilerplate.twitter_accounts
CREATE TABLE IF NOT EXISTS `twitter_accounts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Twitter UserID',
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Twitter Username',
  `trigger` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `access_token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_secret` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('active','suspend') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'suspend',
  `expired_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table boilerplate.twitter_accounts: ~1 rows (approximately)
DELETE FROM `twitter_accounts`;
/*!40000 ALTER TABLE `twitter_accounts` DISABLE KEYS */;
INSERT INTO `twitter_accounts` (`id`, `user_id`, `uid`, `username`, `trigger`, `access_token`, `access_token_secret`, `status`, `expired_date`, `created_at`, `updated_at`) VALUES
	(3, '5', '1128070474397708288', 'socialfess', NULL, '1128070474397708288-sOYj6XN9GP4hCRhy2ITtccHLhPXotS', '1YttggfaIzSJ1GIwtUK5E4Gk4njBAFmbeFff5Thv5hni8', 'suspend', '2019-09-03 09:47:42', '2019-09-03 02:47:42', '2019-09-05 02:09:36'),
	(4, '4', '1168287846513250304', 'sipentolan', NULL, '1168287846513250304-S34EHt55BBJ9Fn6kOFduPCDIW1RqCz', 'WTeDXJHCNHSn0Nf2rAGUmjCiQ5z20OP1JIjmpBUTGkhSM', 'suspend', '2019-09-03 09:50:40', '2019-09-03 02:50:40', '2019-09-03 02:50:40');
/*!40000 ALTER TABLE `twitter_accounts` ENABLE KEYS */;

-- Dumping structure for table boilerplate.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` enum('admin','member') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'member',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table boilerplate.users: ~2 rows (approximately)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `role`, `password`, `avatar`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'Arbha Pradana', 'admin@admin.com', 'admin', '$2y$10$1JI6w2T0t.pDxTHNUt1ws.e6dlm.WnsfKPYZcMv24lQMxmy0GbP1.', NULL, NULL, '2019-08-13 07:21:39', '2019-08-18 10:27:10'),
	(3, 'KampusMF', 'kampusmf@twitter.com', 'member', NULL, 'http://pbs.twimg.com/profile_images/1156428792040026112/pHO23XYA.jpg', NULL, '2019-09-03 02:47:42', '2019-09-03 02:47:42'),
	(4, 'Pentol', 'sipentolan@twitter.com', 'member', NULL, 'http://pbs.twimg.com/profile_images/1168290914902478848/vWMNTnih.jpg', NULL, '2019-09-03 02:50:40', '2019-09-03 02:50:40'),
	(5, 'SOCIALFESS🇲🇨', 'socialfess@twitter.com', 'member', NULL, 'http://pbs.twimg.com/profile_images/1168784408624553984/c2mfkn9C.jpg', NULL, '2019-09-05 02:09:36', '2019-09-05 02:09:36');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
