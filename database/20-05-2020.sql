-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.5.0.5332
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table skripsi-analisis-sentimen.classifications
CREATE TABLE IF NOT EXISTS `classifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tweet` varchar(255) NOT NULL,
  `probNeg` double NOT NULL DEFAULT '0',
  `probPos` decimal(30,30) NOT NULL DEFAULT '0.000000000000000000000000000000',
  `flag` enum('positive','negative') NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC COMMENT='#5 Classification counted from Preprocessing and Training Probabilities table';

-- Dumping data for table skripsi-analisis-sentimen.classifications: ~0 rows (approximately)
DELETE FROM `classifications`;
/*!40000 ALTER TABLE `classifications` DISABLE KEYS */;
/*!40000 ALTER TABLE `classifications` ENABLE KEYS */;

-- Dumping structure for table skripsi-analisis-sentimen.crawlings
CREATE TABLE IF NOT EXISTS `crawlings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tweet` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='#1 Twitter raw data';

-- Dumping data for table skripsi-analisis-sentimen.crawlings: ~0 rows (approximately)
DELETE FROM `crawlings`;
/*!40000 ALTER TABLE `crawlings` DISABLE KEYS */;
/*!40000 ALTER TABLE `crawlings` ENABLE KEYS */;

-- Dumping structure for table skripsi-analisis-sentimen.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table skripsi-analisis-sentimen.migrations: ~0 rows (approximately)
DELETE FROM `migrations`;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table skripsi-analisis-sentimen.preprocessings
CREATE TABLE IF NOT EXISTS `preprocessings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tweet` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC COMMENT='#4 Preprocessing table from all data from Crawlings table';

-- Dumping data for table skripsi-analisis-sentimen.preprocessings: ~0 rows (approximately)
DELETE FROM `preprocessings`;
/*!40000 ALTER TABLE `preprocessings` DISABLE KEYS */;
/*!40000 ALTER TABLE `preprocessings` ENABLE KEYS */;

-- Dumping structure for table skripsi-analisis-sentimen.trainings
CREATE TABLE IF NOT EXISTS `trainings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tweet` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin,
  `flag` varchar(250) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC COMMENT='#2 Training from Crawlings table';

-- Dumping data for table skripsi-analisis-sentimen.trainings: ~0 rows (approximately)
DELETE FROM `trainings`;
/*!40000 ALTER TABLE `trainings` DISABLE KEYS */;
/*!40000 ALTER TABLE `trainings` ENABLE KEYS */;

-- Dumping structure for table skripsi-analisis-sentimen.training_probabilities
CREATE TABLE IF NOT EXISTS `training_probabilities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `word` varchar(255) NOT NULL,
  `probNeg` double NOT NULL DEFAULT '0',
  `probPos` double NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='#3 Probability counted from Trainings table';

-- Dumping data for table skripsi-analisis-sentimen.training_probabilities: ~0 rows (approximately)
DELETE FROM `training_probabilities`;
/*!40000 ALTER TABLE `training_probabilities` DISABLE KEYS */;
/*!40000 ALTER TABLE `training_probabilities` ENABLE KEYS */;

-- Dumping structure for table skripsi-analisis-sentimen.twitter_accounts
CREATE TABLE IF NOT EXISTS `twitter_accounts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Twitter UserID',
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Twitter Username',
  `trigger` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `access_token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_secret` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('active','suspend') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'suspend',
  `expired_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table skripsi-analisis-sentimen.twitter_accounts: ~1 rows (approximately)
DELETE FROM `twitter_accounts`;
/*!40000 ALTER TABLE `twitter_accounts` DISABLE KEYS */;
INSERT INTO `twitter_accounts` (`id`, `user_id`, `uid`, `username`, `trigger`, `access_token`, `access_token_secret`, `status`, `expired_date`, `created_at`, `updated_at`) VALUES
	(2, '3', '1168287846513250304', 'sipentolan', NULL, '1168287846513250304-c25bwe0kn6k4yo3lXcJTs7kwDDF9n9', 'ap3nwk40vtUw9tO1A62ZOrJA1jSkdagd8l3FiK281NQUZ', 'suspend', '2020-04-08 14:28:36', '2020-04-08 07:28:36', '2020-04-08 07:28:36'),
	(3, '4', '1128070474397708288', 'socialfess', NULL, '1128070474397708288-sOYj6XN9GP4hCRhy2ITtccHLhPXotS', '1YttggfaIzSJ1GIwtUK5E4Gk4njBAFmbeFff5Thv5hni8', 'suspend', '2020-05-20 12:49:22', '2020-05-20 05:49:22', '2020-05-20 05:49:22');
/*!40000 ALTER TABLE `twitter_accounts` ENABLE KEYS */;

-- Dumping structure for table skripsi-analisis-sentimen.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` enum('admin','member') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'member',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table skripsi-analisis-sentimen.users: ~3 rows (approximately)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `role`, `password`, `avatar`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'Administrator', 'admin@admin.com', 'admin', '$2y$10$IOGYApdvv81LKtmOIh1RMOHL3ftUdgqL9eM4VeqzIsMIvOp60lw.m', NULL, NULL, '2020-04-08 07:22:32', '2020-05-12 03:32:17'),
	(3, 'Pentol', 'sipentolan@twitter.com', 'member', NULL, 'http://pbs.twimg.com/profile_images/1168290914902478848/vWMNTnih.jpg', NULL, '2020-04-08 07:28:36', '2020-04-08 07:28:36'),
	(4, 'SOCIALFESS🇲🇨 | Base OFF.', 'socialfess@twitter.com', 'member', NULL, 'http://pbs.twimg.com/profile_images/1168784408624553984/c2mfkn9C.jpg', NULL, '2020-05-20 05:49:22', '2020-05-20 05:49:22');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
