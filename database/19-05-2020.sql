-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.5.0.5332
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table skripsi-analisis-sentimen.crawlings
CREATE TABLE IF NOT EXISTS `crawlings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tweet` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=95 DEFAULT CHARSET=latin1;

-- Dumping data for table skripsi-analisis-sentimen.crawlings: ~94 rows (approximately)
DELETE FROM `crawlings`;
/*!40000 ALTER TABLE `crawlings` DISABLE KEYS */;
INSERT INTO `crawlings` (`id`, `tweet`, `created_at`, `updated_at`) VALUES
	(1, 'RT @RestyReseh: Dgn kebijqan ngaco gini #COVID19 akan hilang dari media tp tetal akan mengganas krn rakyat mikir wabah sudah tidak ada .…', '2020-05-19 16:05:17', '2020-05-19 16:05:17'),
	(2, 'Untuk tahun ini terpaksa tidak mudik dulu, mengikuti anjuran pemerintah…semoga segera selesai wabah corona #covid19 #LebaranGarisDepan', '2020-05-19 16:05:17', '2020-05-19 16:05:17'),
	(3, 'Indonesia Butuh 300.000 Petani Untuk Garap Persawahan Baru. Pemerintah akan segera mewujudkan rencana  pengembangan… https://t.co/woJ22AWWOl', '2020-05-19 16:05:17', '2020-05-19 16:05:17'),
	(4, 'RT @BNNCegahNarkoba: Hi #SobatCegah, Social Distancing merupakan salah satu cara pencegahan penularan virus #COVID19 yg dihimbau pemerintah…', '2020-05-19 16:05:17', '2020-05-19 16:05:17'),
	(5, 'Mencanangkan new normal, pemerintah Singapura secara bertahap dan hati-hati akan membuka kembali roda ekonomi dan k… https://t.co/wCFM1YgCdC', '2020-05-19 16:05:17', '2020-05-19 16:05:17'),
	(6, 'RT @GerryS: 14/22: Pemerintah mengharuskan airline² tetap merefund, sementara airline² masih harus bayar gaji karyawan dan sewa pesawat, ta…', '2020-05-19 16:05:17', '2020-05-19 16:05:17'),
	(7, '#IdulFitri #pemerintah #PSBBJakarta #lockdown2020 #COVID19 #waqarzaka', '2020-05-19 16:05:17', '2020-05-19 16:05:17'),
	(8, 'RT @radiotelstar: NU dan Muhammadiyah sepakat pelaksanaan Shalat ID di rumah. MUI dan Pemerintah memboleh kan masyarakat Shalat ID di masji…', '2020-05-19 16:05:17', '2020-05-19 16:05:17'),
	(9, 'Hidup pada mau ditanggung pemerintah, tapi ngikutin aturan pemerintah aja ga bisa\n\n#Covid_19 #COVID19 #dirumahaja… https://t.co/UisuzYU7LT', '2020-05-19 16:05:17', '2020-05-19 16:05:17'),
	(10, 'RT @RestyReseh: Dgn kebijqan ngaco gini #COVID19 akan hilang dari media tp tetal akan mengganas krn rakyat mikir wabah sudah tidak ada .…', '2020-05-19 16:05:17', '2020-05-19 16:05:17'),
	(11, 'RT @GerryS: 14/22: Pemerintah mengharuskan airline² tetap merefund, sementara airline² masih harus bayar gaji karyawan dan sewa pesawat, ta…', '2020-05-19 16:05:17', '2020-05-19 16:05:17'),
	(12, 'RT @RECOFTCIndo: Untuk mengurangi konflik lahan di #Asia selama pandemi #Covid19, pemerintah perlu memastikan hak masyarakat, termasuk kelo…', '2020-05-19 16:05:17', '2020-05-19 16:05:17'),
	(13, 'RT @GerryS: 14/22: Pemerintah mengharuskan airline² tetap merefund, sementara airline² masih harus bayar gaji karyawan dan sewa pesawat, ta…', '2020-05-19 16:05:17', '2020-05-19 16:05:17'),
	(14, 'RT @BaleBengong: Kalau hanya mendengar versi pemerintah, penanganan #COVID19 di Bali ini memang terlihat bagus. Beda lagi kalau tanya ke ah…', '2020-05-19 16:05:17', '2020-05-19 16:05:17'),
	(15, 'Untuk mengurangi konflik lahan di #Asia selama pandemi #Covid19, pemerintah perlu memastikan hak masyarakat, termas… https://t.co/qvwz411Szc', '2020-05-19 16:05:17', '2020-05-19 16:05:17'),
	(16, 'Pemerintah kalau mau penyebaran #Covid19 ini cepat selesai ya jangan setengah-setengah. Lockdown sekalian, spread v… https://t.co/sQN2pcnJTI', '2020-05-19 16:05:17', '2020-05-19 16:05:17'),
	(17, 'Ketua MPR Minta Pemerintah Konsisten Soal PSBB\n\n#MPR #PSBB #coronavirus #Corona #COVID #COVID19 #COVIDー19… https://t.co/Z8ctP7nzfT', '2020-05-19 16:05:17', '2020-05-19 16:05:17'),
	(18, 'RT @infodrakor_id: Dispatch ungkap genk anggota 97 liners: #JUNGKOOK #BTS, #CHAEUNWOO #Astro #JAEHYUN #NCT dan #KIMMINGYU #Seveteen berkunj…', '2020-05-19 16:05:18', '2020-05-19 16:05:18'),
	(19, 'RT @RestyReseh: Dgn kebijqan ngaco gini #COVID19 akan hilang dari media tp tetal akan mengganas krn rakyat mikir wabah sudah tidak ada .…', '2020-05-19 16:05:18', '2020-05-19 16:05:18'),
	(20, 'RT @RestyReseh: Dgn kebijqan ngaco gini #COVID19 akan hilang dari media tp tetal akan mengganas krn rakyat mikir wabah sudah tidak ada .…', '2020-05-19 16:05:18', '2020-05-19 16:05:18'),
	(21, 'RT @GerryS: 14/22: Pemerintah mengharuskan airline² tetap merefund, sementara airline² masih harus bayar gaji karyawan dan sewa pesawat, ta…', '2020-05-19 16:05:18', '2020-05-19 16:05:18'),
	(22, 'RT @CrowDits: Hampir seminggu para tenaga medis China tiba di Italia utk mengurangi wabah #COVID19. Tp mereka keliatan putus asa. Social di…', '2020-05-19 16:05:18', '2020-05-19 16:05:18'),
	(23, 'RT @BKFKemenkeu: Program Pemulihan Ekonomi Nasional (PEN) bertujuan untuk membantu dunia usaha termasuk UMKM untuk bertahan di tengah ketid…', '2020-05-19 16:05:18', '2020-05-19 16:05:18'),
	(24, 'Walaupun banyak pro dan kontra tentang kebijakan ini, politisi Partai Golkar meyakini pemerintah telah mendengar da… https://t.co/cT63ts9AUI', '2020-05-19 16:05:18', '2020-05-19 16:05:18'),
	(25, '4. Penularan #COVID19 masih terjadi ditengah-tengah masyarakat, diperlukan kesungguhan dan kepatuhan untuk menjalan… https://t.co/HSZJIZIlAU', '2020-05-19 16:05:18', '2020-05-19 16:05:18'),
	(26, '@Rerie_Moerdijat Mari kita patuhi aturan pemerintah untuk memutus rantai penyebaran #Covid19 agar pandemi ini dapat… https://t.co/shWhrBDySI', '2020-05-19 16:05:18', '2020-05-19 16:05:18'),
	(27, 'RT @Rerie_Moerdijat: 8. Sangat penting untuk membangun kesadaran masyarakat bahwa wabah #Covid19 ini harus dihadapi secara bersama dengan d…', '2020-05-19 16:05:18', '2020-05-19 16:05:18'),
	(28, 'RT @catchmeupid: Pemerintah mengumumkan jumlah kasus #COVID19 di Indonesia tanggal  19 Mei 2020:\n\nTotal: 18.496 (+486)\n- Dalam perawatan:…', '2020-05-19 16:05:18', '2020-05-19 16:05:18'),
	(29, 'RT @GerryS: 14/22: Pemerintah mengharuskan airline² tetap merefund, sementara airline² masih harus bayar gaji karyawan dan sewa pesawat, ta…', '2020-05-19 16:05:18', '2020-05-19 16:05:18'),
	(30, 'RT @catchmeupid: Pemerintah mengumumkan jumlah kasus #COVID19 di Indonesia tanggal  19 Mei 2020:\n\nTotal: 18.496 (+486)\n- Dalam perawatan:…', '2020-05-19 16:05:18', '2020-05-19 16:05:18'),
	(31, 'RT @Rerie_Moerdijat: 8. Sangat penting untuk membangun kesadaran masyarakat bahwa wabah #Covid19 ini harus dihadapi secara bersama dengan d…', '2020-05-19 16:05:18', '2020-05-19 16:05:18'),
	(32, 'RT @catchmeupid: Pemerintah mengumumkan jumlah kasus #COVID19 di Indonesia tanggal  19 Mei 2020:\n\nTotal: 18.496 (+486)\n- Dalam perawatan:…', '2020-05-19 16:05:18', '2020-05-19 16:05:18'),
	(33, 'Program Pemulihan Ekonomi Nasional (PEN) bertujuan untuk membantu dunia usaha termasuk UMKM untuk bertahan di tenga… https://t.co/uWwYUkKNAS', '2020-05-19 16:05:18', '2020-05-19 16:05:18'),
	(34, 'Pemerintah mengumumkan jumlah kasus #COVID19 di Indonesia tanggal  19 Mei 2020:\n\nTotal: 18.496 (+486)\n- Dalam peraw… https://t.co/V1XpFmvb1I', '2020-05-19 16:05:18', '2020-05-19 16:05:18'),
	(35, 'RT @GerryS: 14/22: Pemerintah mengharuskan airline² tetap merefund, sementara airline² masih harus bayar gaji karyawan dan sewa pesawat, ta…', '2020-05-19 16:05:18', '2020-05-19 16:05:18'),
	(36, 'RT @harmoni3roda: 1. Sahabat Harmoni\nDemi mendukung keberhasilan program pencegahan penyebaran Covid-19 pemerintah, Indocement Kompleks Tar…', '2020-05-19 16:05:18', '2020-05-19 16:05:18'),
	(37, 'RT @KompasData: Kegagalan pembatasan sosial berskala besar bakal memicu ledakan kasus Covid-19 di Indonesia. Karena itu, pemerintah mesti m…', '2020-05-19 16:05:18', '2020-05-19 16:05:18'),
	(38, 'RT @pmintb_: Senin (18/5/2020) PMI &amp; @bpbd_ntb melakukan siaran radio melalui @RRI untuk mengajak masyarakat #tundamudik demi memutus mata…', '2020-05-19 16:05:18', '2020-05-19 16:05:18'),
	(39, 'RT @Rerie_Moerdijat: 8. Sangat penting untuk membangun kesadaran masyarakat bahwa wabah #Covid19 ini harus dihadapi secara bersama dengan d…', '2020-05-19 16:05:18', '2020-05-19 16:05:18'),
	(40, 'RT @BaleBengong: Kalau hanya mendengar versi pemerintah, penanganan #COVID19 di Bali ini memang terlihat bagus. Beda lagi kalau tanya ke ah…', '2020-05-19 16:05:18', '2020-05-19 16:05:18'),
	(41, 'RT @Rerie_Moerdijat: 8. Sangat penting untuk membangun kesadaran masyarakat bahwa wabah #Covid19 ini harus dihadapi secara bersama dengan d…', '2020-05-19 16:05:18', '2020-05-19 16:05:18'),
	(42, 'RT @berlianidris: Reportase keren oleh (salah satunya) sahabat saya Hellena @sweethellena tentang bagaimana Pemerintah menangani #Covid19.…', '2020-05-19 16:05:18', '2020-05-19 16:05:18'),
	(43, 'RT @Rerie_Moerdijat: 8. Sangat penting untuk membangun kesadaran masyarakat bahwa wabah #Covid19 ini harus dihadapi secara bersama dengan d…', '2020-05-19 16:05:18', '2020-05-19 16:05:18'),
	(44, 'RT @Rerie_Moerdijat: 8. Sangat penting untuk membangun kesadaran masyarakat bahwa wabah #Covid19 ini harus dihadapi secara bersama dengan d…', '2020-05-19 16:05:18', '2020-05-19 16:05:18'),
	(45, '@BKNgoid Kenapa tidak didahulukan dulu penanganan #COVID19  sampai tuntas? Rasanya pandemi #COVID19 lebih urgen. Be… https://t.co/RtzG3fQK0r', '2020-05-19 16:05:19', '2020-05-19 16:05:19'),
	(46, 'Menteri BUMN, Erick Thohir menegaskan bahwa karyawan BUMN akan tetap libur di hari lebaran sesuai keputusan pemerin… https://t.co/bCtgWqgRQI', '2020-05-19 16:05:19', '2020-05-19 16:05:19'),
	(47, 'RT @Rerie_Moerdijat: 8. Sangat penting untuk membangun kesadaran masyarakat bahwa wabah #Covid19 ini harus dihadapi secara bersama dengan d…', '2020-05-19 16:05:19', '2020-05-19 16:05:19'),
	(48, 'RT @Rerie_Moerdijat: 8. Sangat penting untuk membangun kesadaran masyarakat bahwa wabah #Covid19 ini harus dihadapi secara bersama dengan d…', '2020-05-19 16:05:19', '2020-05-19 16:05:19'),
	(49, 'RT @Rerie_Moerdijat: 8. Sangat penting untuk membangun kesadaran masyarakat bahwa wabah #Covid19 ini harus dihadapi secara bersama dengan d…', '2020-05-19 16:05:19', '2020-05-19 16:05:19'),
	(50, 'RT @RAMujiyono: What the Maksud nih? Pak @jokowi ? Sudah semakin membaikkah? Ini kah yg dimaksud berdamai dengan #COVID19 ? Cc @aniesbaswed…', '2020-05-19 16:05:19', '2020-05-19 16:05:19'),
	(51, 'RT @Rerie_Moerdijat: 8. Sangat penting untuk membangun kesadaran masyarakat bahwa wabah #Covid19 ini harus dihadapi secara bersama dengan d…', '2020-05-19 16:05:19', '2020-05-19 16:05:19'),
	(52, 'RT @RestyReseh: Dgn kebijqan ngaco gini #COVID19 akan hilang dari media tp tetal akan mengganas krn rakyat mikir wabah sudah tidak ada .…', '2020-05-19 16:05:19', '2020-05-19 16:05:19'),
	(53, 'RT @Rerie_Moerdijat: 8. Sangat penting untuk membangun kesadaran masyarakat bahwa wabah #Covid19 ini harus dihadapi secara bersama dengan d…', '2020-05-19 16:05:19', '2020-05-19 16:05:19'),
	(54, 'Please,\nUntuk saat ini hindari kerumunan.\nPatuhi kebijakan pemerintah,dan hargai perjuangan tenaga medis.\nTolong ke… https://t.co/na3aNleFOD', '2020-05-19 16:05:19', '2020-05-19 16:05:19'),
	(55, '#semetonbawaslu berikut update perkembangan Covid-19 di Kabupaten Bangli per-tanggal 19 Mei 2020\n\nSemangat saudarak… https://t.co/abFJ1BiYUc', '2020-05-19 16:05:19', '2020-05-19 16:05:19'),
	(56, 'Kegagalan pembatasan sosial berskala besar bakal memicu ledakan kasus Covid-19 di Indonesia. Karena itu, pemerintah… https://t.co/xxK1L7vjiv', '2020-05-19 16:05:19', '2020-05-19 16:05:19'),
	(57, 'Semoga pemerintah segera tak lengah atasi krisis pangan negeri.\n\n#FutureOfFood #ZeroHunger #ketahananpangan… https://t.co/dyvXn5zwa0', '2020-05-19 16:05:19', '2020-05-19 16:05:19'),
	(58, '14/22: Pemerintah mengharuskan airline² tetap merefund, sementara airline² masih harus bayar gaji karyawan dan sewa… https://t.co/MLxjAUAzoP', '2020-05-19 16:05:19', '2020-05-19 16:05:19'),
	(59, 'RT @FananiRob: Dilema hari raya dan covid19, belum lagi di kota yang lainnya.. Mengingatkan Jangan lupa masker dan himbauan pemerintah yang…', '2020-05-19 16:05:19', '2020-05-19 16:05:19'),
	(60, 'Update Data! Media Centre #Covid19 Tanggal 19 Mei 2020 \n\nWebsite resmi Pemerintah Provinsi Sumatera Utara untuk mel… https://t.co/U02RwsUtVk', '2020-05-19 16:05:19', '2020-05-19 16:05:19'),
	(61, 'Update Data! Media Centre #Covid19 Tanggal 19 Mei 2020 \n\nWebsite resmi Pemerintah Provinsi Sumatera Utara untuk mel… https://t.co/ig7KaglJgu', '2020-05-19 16:05:19', '2020-05-19 16:05:19'),
	(62, 'Update Data! Media Centre #Covid19 Tanggal 19 Mei 2020 \n\nWebsite resmi Pemerintah Provinsi Sumatera Utara untuk mel… https://t.co/Oyo7DsaFv5', '2020-05-19 16:05:19', '2020-05-19 16:05:19'),
	(63, 'RT @mnctrijayafm: Pemerintah mengonfirmasi ada penambahan 486 pasien positif Corona pada Selasa (19/5/2020). Dengan begitu, total pasien po…', '2020-05-19 16:05:19', '2020-05-19 16:05:19'),
	(64, 'kita gak bisa tinggal diem aja ngeliat semua sodara-sodara kita yang sedang menderita merasakan dampak dari pandemi… https://t.co/XZYO4AdquJ', '2020-05-19 16:05:19', '2020-05-19 16:05:19'),
	(65, '@A2108L @habiburokhman Kalo cm dpt info ttg #COVID19 dr pmerintah lwt TVRI/ TV2 Swasta atau mainstream media Indone… https://t.co/MjxajsEGaQ', '2020-05-19 16:05:19', '2020-05-19 16:05:19'),
	(66, '@CNNIndonesia Pemerintah @jokowi pd posisi Serba Salah, krn telat antisipasi n misKordinasi di Kabinet, ahkirnya pe… https://t.co/kJeUbNSM01', '2020-05-19 16:05:19', '2020-05-19 16:05:19'),
	(67, 'RT @mnctrijayafm: Pemerintah mengonfirmasi ada penambahan 486 pasien positif Corona pada Selasa (19/5/2020). Dengan begitu, total pasien po…', '2020-05-19 16:05:19', '2020-05-19 16:05:19'),
	(68, 'Pemerintah mengonfirmasi ada penambahan 486 pasien positif Corona pada Selasa (19/5/2020). Dengan begitu, total pas… https://t.co/hIAFxBDuTM', '2020-05-19 16:05:19', '2020-05-19 16:05:19'),
	(69, '@ahsyahroni_ Makasih infonya 👍\n#indonesiaterserah #pemerintah #COVID19 #COVID19indonesia #Ramadhan', '2020-05-19 16:05:19', '2020-05-19 16:05:19'),
	(70, '8. Sangat penting untuk membangun kesadaran masyarakat bahwa wabah #Covid19 ini harus dihadapi secara bersama denga… https://t.co/N3CNZMVpw6', '2020-05-19 16:05:20', '2020-05-19 16:05:20'),
	(71, 'Tolong dong, kpd pemerintah, semua yang melanggar PSBB dikarantina aja #psbb #COVID19 #bnpb', '2020-05-19 16:05:20', '2020-05-19 16:05:20'),
	(72, 'RT @gataubingungehe: Bantu viralin orang macam begini, biar ditindak lanjutin, meremehkan covid-19 dan menyepelekan anjuran pemerintah tent…', '2020-05-19 16:05:20', '2020-05-19 16:05:20'),
	(73, 'RT @PemkotMalang: #NawakNgalam, Pemerintah Kota Malang telah menyiapkan rumah karantina sebagai bentuk keseriusan Kota Malang dalam melawan…', '2020-05-19 16:05:20', '2020-05-19 16:05:20'),
	(74, 'RT @RestyReseh: Dgn kebijqan ngaco gini #COVID19 akan hilang dari media tp tetal akan mengganas krn rakyat mikir wabah sudah tidak ada .…', '2020-05-19 16:05:20', '2020-05-19 16:05:20'),
	(75, 'RT @berlianidris: Reportase keren oleh (salah satunya) sahabat saya Hellena @sweethellena tentang bagaimana Pemerintah menangani #Covid19.…', '2020-05-19 16:05:20', '2020-05-19 16:05:20'),
	(76, '#StayHomeStaySafe #tenagamedis #indonesiabisa #indonesiasehat #COVID19 #dirumahsaja please kalian diam dirumah kasi… https://t.co/NeMH1scQg2', '2020-05-19 16:05:20', '2020-05-19 16:05:20'),
	(77, 'Kunci keberhasilan pengendalian penyebaran #COVID19 adlh gerak cepat, ketegasan &amp; kekonsistenan pemerintah dlm meng… https://t.co/yHKDQJA7BS', '2020-05-19 16:05:20', '2020-05-19 16:05:20'),
	(78, 'RT @DitjenKN: Yang perlu #SobatKaeN ketahui, tidak semua BUMN mendapatkan PMN.\nDalam penyaluran PMN ini, Pemerintah tentu akan sangat selek…', '2020-05-19 16:05:20', '2020-05-19 16:05:20'),
	(79, 'https://t.co/vaZLwCTAxI - Pemerintah Kabupaten Purworejo mulai menyalurkan bantuan sosial bagi warga masyarakat ter… https://t.co/e99WSYTAH6', '2020-05-19 16:05:20', '2020-05-19 16:05:20'),
	(80, 'RT @RestyReseh: Dgn kebijqan ngaco gini #COVID19 akan hilang dari media tp tetal akan mengganas krn rakyat mikir wabah sudah tidak ada .…', '2020-05-19 16:05:20', '2020-05-19 16:05:20'),
	(81, 'RT @tvOneNews: Pemerintah setempat menutup seluruh aktivitas warga setelah peristiwa pembukaan bungkus jenazah pasien positif Covid-19 di S…', '2020-05-19 16:05:20', '2020-05-19 16:05:20'),
	(82, 'RT @TETO_Jakarta: @detikcom @detikfinance Taiwan🇹🇼 sangat bersedia untuk berbagi pengalaman pencegahan pandemi #COVID19 dengan Indonesia🇮🇩.…', '2020-05-19 16:05:20', '2020-05-19 16:05:20'),
	(83, 'Lama-lama gerah juga sama kebijakan pemerintah dan framing media2 mainstream. \n\n#COVID19 \n#indonesiaterserah', '2020-05-19 16:05:20', '2020-05-19 16:05:20'),
	(84, 'RT @pmintb_: Senin (18/5/2020) PMI &amp; @bpbd_ntb melakukan siaran radio melalui @RRI untuk mengajak masyarakat #tundamudik demi memutus mata…', '2020-05-19 16:05:20', '2020-05-19 16:05:20'),
	(85, 'RT @RestyReseh: Dgn kebijqan ngaco gini #COVID19 akan hilang dari media tp tetal akan mengganas krn rakyat mikir wabah sudah tidak ada .…', '2020-05-19 16:05:20', '2020-05-19 16:05:20'),
	(86, 'RT @RestyReseh: Dgn kebijqan ngaco gini #COVID19 akan hilang dari media tp tetal akan mengganas krn rakyat mikir wabah sudah tidak ada .…', '2020-05-19 16:05:20', '2020-05-19 16:05:20'),
	(87, '@detikcom @detikfinance Taiwan🇹🇼 sangat bersedia untuk berbagi pengalaman pencegahan pandemi #COVID19 dengan Indone… https://t.co/8SZNNLmrTY', '2020-05-19 16:05:20', '2020-05-19 16:05:20'),
	(88, 'RT @Rerie_Moerdijat: 9. Jika ingin melonggarkan kebijakan PSBB, pemerintah perlu memastikan kita telah melewati puncak pandemi #Covid19.\n\n#…', '2020-05-19 16:05:20', '2020-05-19 16:05:20'),
	(89, 'RT @Rerie_Moerdijat: 7. Oleh karena itu, pemerintah perlu meninjau kembali isu terkait pelonggaran kebijakan PSBB. Lebih baik meningkatkan…', '2020-05-19 16:05:20', '2020-05-19 16:05:20'),
	(90, 'RT @RestyReseh: Dgn kebijqan ngaco gini #COVID19 akan hilang dari media tp tetal akan mengganas krn rakyat mikir wabah sudah tidak ada .…', '2020-05-19 16:05:20', '2020-05-19 16:05:20'),
	(91, 'Pemerintah Kota Bekasi hanya memperbolehkan zona hijau menggelar salat Idul Fitri. Sedangkan, untuk zona merah dimi… https://t.co/RCIXSe4js8', '2020-05-19 16:05:20', '2020-05-19 16:05:20'),
	(92, 'RT @ogysugiyono: Lewati #COVID19 Politikus @DPP_PKB Minta Pemerintah @jokowi Investasi Kesehatan @KemenkesRI @KemenBUMN @erickthohir @airla…', '2020-05-19 16:05:20', '2020-05-19 16:05:20'),
	(93, 'RT @Rerie_Moerdijat: 9. Jika ingin melonggarkan kebijakan PSBB, pemerintah perlu memastikan kita telah melewati puncak pandemi #Covid19.\n\n#…', '2020-05-19 16:05:20', '2020-05-19 16:05:20'),
	(94, 'RT @Rerie_Moerdijat: 7. Oleh karena itu, pemerintah perlu meninjau kembali isu terkait pelonggaran kebijakan PSBB. Lebih baik meningkatkan…', '2020-05-19 16:05:20', '2020-05-19 16:05:20');
/*!40000 ALTER TABLE `crawlings` ENABLE KEYS */;

-- Dumping structure for table skripsi-analisis-sentimen.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table skripsi-analisis-sentimen.migrations: ~0 rows (approximately)
DELETE FROM `migrations`;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Dumping structure for table skripsi-analisis-sentimen.trainings
CREATE TABLE IF NOT EXISTS `trainings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tweet` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin,
  `flag` enum('positif','negatif') DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- Dumping data for table skripsi-analisis-sentimen.trainings: ~94 rows (approximately)
DELETE FROM `trainings`;
/*!40000 ALTER TABLE `trainings` DISABLE KEYS */;
INSERT INTO `trainings` (`id`, `tweet`, `flag`, `created_at`, `updated_at`) VALUES
	(2, 'Untuk tahun ini terpaksa tidak mudik dulu, mengikuti anjuran pemerintah…semoga segera selesai wabah corona #covid19 #LebaranGarisDepan', 'positif', '2020-05-19 16:05:17', '2020-05-19 16:05:17'),
	(3, 'Indonesia Butuh 300.000 Petani Untuk Garap Persawahan Baru. Pemerintah akan segera mewujudkan rencana  pengembangan… https://t.co/woJ22AWWOl', 'positif', '2020-05-19 16:05:17', '2020-05-19 16:05:17'),
	(9, 'Hidup pada mau ditanggung pemerintah, tapi ngikutin aturan pemerintah aja ga bisa\n\n#Covid_19 #COVID19 #dirumahaja… https://t.co/UisuzYU7LT', 'negatif', '2020-05-19 16:05:17', '2020-05-19 16:05:17'),
	(16, 'Pemerintah kalau mau penyebaran #Covid19 ini cepat selesai ya jangan setengah-setengah. Lockdown sekalian, spread v… https://t.co/sQN2pcnJTI', 'negatif', '2020-05-19 16:05:17', '2020-05-19 16:05:17'),
	(17, 'Ketua MPR Minta Pemerintah Konsisten Soal PSBB\n\n#MPR #PSBB #coronavirus #Corona #COVID #COVID19 #COVIDー19… https://t.co/Z8ctP7nzfT', 'positif', '2020-05-19 16:05:17', '2020-05-19 16:05:17'),
	(24, 'Walaupun banyak pro dan kontra tentang kebijakan ini, politisi Partai Golkar meyakini pemerintah telah mendengar da… https://t.co/cT63ts9AUI', 'positif', '2020-05-19 16:05:18', '2020-05-19 16:05:18'),
	(25, '4. Penularan #COVID19 masih terjadi ditengah-tengah masyarakat, diperlukan kesungguhan dan kepatuhan untuk menjalan… https://t.co/HSZJIZIlAU', 'positif', '2020-05-19 16:05:18', '2020-05-19 16:05:18'),
	(26, '@Rerie_Moerdijat Mari kita patuhi aturan pemerintah untuk memutus rantai penyebaran #Covid19 agar pandemi ini dapat… https://t.co/shWhrBDySI', 'positif', '2020-05-19 16:05:18', '2020-05-19 16:05:18'),
	(33, 'Program Pemulihan Ekonomi Nasional (PEN) bertujuan untuk membantu dunia usaha termasuk UMKM untuk bertahan di tenga… https://t.co/uWwYUkKNAS', 'positif', '2020-05-19 16:05:18', '2020-05-19 16:05:18'),
	(45, '@BKNgoid Kenapa tidak didahulukan dulu penanganan #COVID19  sampai tuntas? Rasanya pandemi #COVID19 lebih urgen. Be… https://t.co/RtzG3fQK0r', 'negatif', '2020-05-19 16:05:19', '2020-05-19 16:05:19'),
	(46, 'Menteri BUMN, Erick Thohir menegaskan bahwa karyawan BUMN akan tetap libur di hari lebaran sesuai keputusan pemerin… https://t.co/bCtgWqgRQI', 'positif', '2020-05-19 16:05:19', '2020-05-19 16:05:19'),
	(54, 'Please,\nUntuk saat ini hindari kerumunan.\nPatuhi kebijakan pemerintah,dan hargai perjuangan tenaga medis.\nTolong ke… https://t.co/na3aNleFOD', 'positif', '2020-05-19 16:05:19', '2020-05-19 16:05:19'),
	(56, 'Kegagalan pembatasan sosial berskala besar bakal memicu ledakan kasus Covid-19 di Indonesia. Karena itu, pemerintah… https://t.co/xxK1L7vjiv', 'positif', '2020-05-19 16:05:19', '2020-05-19 16:05:19'),
	(58, '14/22: Pemerintah mengharuskan airline² tetap merefund, sementara airline² masih harus bayar gaji karyawan dan sewa… https://t.co/MLxjAUAzoP', 'positif', '2020-05-19 16:05:19', '2020-05-19 16:05:19'),
	(64, 'kita gak bisa tinggal diem aja ngeliat semua sodara-sodara kita yang sedang menderita merasakan dampak dari pandemi… https://t.co/XZYO4AdquJ', 'positif', '2020-05-19 16:05:19', '2020-05-19 16:05:19'),
	(71, 'Tolong dong, kpd pemerintah, semua yang melanggar PSBB dikarantina aja #psbb #COVID19 #bnpb', 'positif', '2020-05-19 16:05:20', '2020-05-19 16:05:20'),
	(77, 'Kunci keberhasilan pengendalian penyebaran #COVID19 adlh gerak cepat, ketegasan &amp; kekonsistenan pemerintah dlm meng… https://t.co/yHKDQJA7BS', 'positif', '2020-05-19 16:05:20', '2020-05-19 16:05:20'),
	(83, 'Lama-lama gerah juga sama kebijakan pemerintah dan framing media2 mainstream. \n\n#COVID19 \n#indonesiaterserah', 'positif', '2020-05-19 16:05:20', '2020-05-19 16:05:20'),
	(91, 'Pemerintah Kota Bekasi hanya memperbolehkan zona hijau menggelar salat Idul Fitri. Sedangkan, untuk zona merah dimi… https://t.co/RCIXSe4js8', 'positif', '2020-05-19 16:05:20', '2020-05-19 16:05:20');
/*!40000 ALTER TABLE `trainings` ENABLE KEYS */;

-- Dumping structure for table skripsi-analisis-sentimen.twitter_accounts
CREATE TABLE IF NOT EXISTS `twitter_accounts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Twitter UserID',
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Twitter Username',
  `trigger` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `access_token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_secret` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('active','suspend') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'suspend',
  `expired_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table skripsi-analisis-sentimen.twitter_accounts: ~0 rows (approximately)
DELETE FROM `twitter_accounts`;
/*!40000 ALTER TABLE `twitter_accounts` DISABLE KEYS */;
INSERT INTO `twitter_accounts` (`id`, `user_id`, `uid`, `username`, `trigger`, `access_token`, `access_token_secret`, `status`, `expired_date`, `created_at`, `updated_at`) VALUES
	(2, '3', '1168287846513250304', 'sipentolan', NULL, '1168287846513250304-c25bwe0kn6k4yo3lXcJTs7kwDDF9n9', 'ap3nwk40vtUw9tO1A62ZOrJA1jSkdagd8l3FiK281NQUZ', 'suspend', '2020-04-08 14:28:36', '2020-04-08 07:28:36', '2020-04-08 07:28:36');
/*!40000 ALTER TABLE `twitter_accounts` ENABLE KEYS */;

-- Dumping structure for table skripsi-analisis-sentimen.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` enum('admin','member') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'member',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table skripsi-analisis-sentimen.users: ~2 rows (approximately)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `role`, `password`, `avatar`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'Administrator', 'admin@admin.com', 'admin', '$2y$10$IOGYApdvv81LKtmOIh1RMOHL3ftUdgqL9eM4VeqzIsMIvOp60lw.m', NULL, NULL, '2020-04-08 07:22:32', '2020-05-12 03:32:17'),
	(3, 'Pentol', 'sipentolan@twitter.com', 'member', NULL, 'http://pbs.twimg.com/profile_images/1168290914902478848/vWMNTnih.jpg', NULL, '2020-04-08 07:28:36', '2020-04-08 07:28:36');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
