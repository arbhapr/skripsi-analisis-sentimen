<?php

namespace App\Services;
use App\Models\TwitterAccount;
use App\User;
use Auth;
use Laravel\Socialite\Contracts\User as ProviderUser;

class TwitterAuth
{
    public function createOrGetUser(ProviderUser $providerUser)
    {
        $newUser     = User::firstOrCreate(
                        [
                            'email' => strtolower($providerUser->nickname).'@twitter.com',
                        ],
                        [
                            'name' => $providerUser->name,
                            'role' => 'member',
                            'avatar' => $providerUser->avatar_original,
                        ]);
        $twitter     =  TwitterAccount::updateOrCreate(
                        [
                            'uid' => $providerUser->id,
                        ],
                        [
                            'user_id' => $newUser->id,
                            'username' => $providerUser->nickname,
                            'access_token' => $providerUser->token,
                            'access_token_secret' => $providerUser->tokenSecret,
                        ]
                    );
        return $newUser;
    }
}