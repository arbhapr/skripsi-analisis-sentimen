<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Probability extends Model
{
    protected $table = 'training_probabilities';
}
