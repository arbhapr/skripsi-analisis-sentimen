<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TwitterAccount extends Model
{
    protected $table = 'twitter_accounts';
    
    protected $fillable = [
        'user_id', 'uid', 'username',
        'access_token', 'access_token_secret',
    ];

    public function toUser()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function toPicker()
    {
        return $this->hasMany('App\Models\Picker', 'user_id', 'id');
    }

    public function toTweet()
    {
        return $this->hasMany('App\Models\Tweets', 'twitter_id', 'id');
    }
}
