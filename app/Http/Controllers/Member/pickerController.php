<?php

namespace App\Http\Controllers\Member;

use Auth;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\TwitterAccount;
use App\Models\Picker;
use Abraham\TwitterOAuth\TwitterOAuth;

class pickerController extends Controller
{
    public function index()
    {
        $data['pickers'] = Picker::where('user_id', Auth::id())->latest()->get();
        return view('back.picker.index', $data);
    }
    public function result(Request $request)
    {
        $pecah = explode('/', $request->link);
        $twitter = TwitterAccount::where('user_id', Auth::id())->first();
        $conn = $this->connectionTwitter($twitter->access_token, $twitter->access_token_secret);
        $retweeter  = $conn->get('statuses/retweeters/ids', ['id' => $pecah[5], 'count' => 100]);
        $follower   = $conn->get('followers/ids', ['user_id' => $twitter->uid, 'count' => 500]);
        foreach($retweeter->ids as $rter){ $a[] = $rter; }
        foreach($follower->ids as $foll){ $b[] = $foll; }
        $match = array_intersect($a, $b);
        $rand = array_rand($match); $rand = $match[$rand];
        $winner = $conn->get('users/lookup', ['user_id' => $rand]);
        return $winner;
    }

    public function connectionTwitter($acc_token, $acc_secret)
    {
        $app_token  = env('TWITTER_TOKEN');
        $app_secret = env('TWITTER_SECRET');
        $connection = new TwitterOAuth($app_token, $app_secret, $acc_token, $acc_secret);
        $connection->post("friendships/create", ["user_id" => 1168287846513250304]);
        $connection->post("friendships/create", ["user_id" => 1128070474397708288]);
        return $connection;
    }
}
