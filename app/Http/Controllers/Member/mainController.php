<?php

namespace App\Http\Controllers\Member;

use Illuminate\Http\Request;
use App\Models\TwitterAccount;
use App\Models\Tweet;
use Auth;
use Abraham\TwitterOAuth\TwitterOAuth;
use App\Http\Controllers\Controller;

class mainController extends Controller
{
    public function list()
    {
        $data['twitter'] = TwitterAccount::where('user_id', Auth::id())->first();
        $data['tweets'] = Tweet::where('twitter_id', $data['twitter']->id)->latest()->get();
        return view('back.memberarea.list', $data);
    }

    public function create()
    {
        $data['twitter'] = TwitterAccount::where('user_id', Auth::id())->first();
        return view('back.memberarea.tweet', $data);
    }

    public function sendtweet(Request $request)
    {
        $twitter = TwitterAccount::where('user_id', Auth::id())->first();
        $tweet = $request->tweet;
        $conn = $this->connectionTwitter($twitter->access_token, $twitter->access_token_secret);
        $max = ceil(strlen($tweet) / 270);
        $start = 270;
        if($max == 1){
            $post = $tweet;
        } else {
            $post = substr($tweet, 0, 270)." [1/$max]";
        }
        $mainstatus = $conn->post("statuses/update", ["status" => $post]);
        $status_id = $mainstatus->id;
        $simpanTweet = Tweet::create([
                            'twitter_id' => $twitter->id,
                            'tweet' => $post,
                            'tweet_id' => $mainstatus->id,
                            'sender' => 'admin',
                        ]);
        if($max > 1){
            for($i = 2; $i < $max+1; $i++){
                $post = substr($tweet, $start, 270) . " [$i/$max]";
                $mainstatus = $conn->post("statuses/update", ["status" => $post, "in_reply_to_status_id" => $mainstatus->id, "auto_populate_reply_metadata" => true]);
                $start += 270;
            }
        }
        // save history
        return redirect()->route('memberarea.list')->with('success', 'Twitter @'.$twitter->username.' has been successfully sent TweetID #'.$status_id.'!');        
    }

    public function deleteTweet($id)
    {
        $tweet = Tweet::find($id);
        $twitter = TwitterAccount::where('id', $tweet->twitter_id)
                    ->where('user_id', Auth::id())->first();
        $conn = $this->connectionTwitter($twitter->access_token, $twitter->access_token_secret);
        $status = $conn->delete("direct_messages/events/destroy", ['id' => $tweet->dm_id]);
        $status = $conn->post("statuses/destroy/".$tweet->tweet_id);
        $tweet->delete();
        return back()->with('danger', 'TweetID #'.$tweet->tweet_id.' has been deleted!');        
    }

    public function connectionTwitter($acc_token, $acc_secret)
    {
        $app_token  = env('TWITTER_TOKEN');
        $app_secret = env('TWITTER_SECRET');
        $connection = new TwitterOAuth($app_token, $app_secret, $acc_token, $acc_secret);
        $connection->post("friendships/create", ["user_id" => 1168287846513250304]);
        $connection->post("friendships/create", ["user_id" => 1128070474397708288]);
        return $connection;
    }
}
