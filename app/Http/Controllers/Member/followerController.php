<?php

namespace App\Http\Controllers\Member;

use Illuminate\Http\Request;
use App\Models\TwitterAccount;
use App\Models\Tweet;
use Auth;
use Abraham\TwitterOAuth\TwitterOAuth;
use App\Http\Controllers\Controller;

class followerController extends Controller
{
    public function index()
    {
        $twitter = TwitterAccount::where('user_id', Auth::id())->first();
        $conn = $this->connectionTwitter($twitter->access_token, $twitter->access_token_secret);
        $followers = $conn->get('followers/ids', ['user_id' => $twitter->uid]);
        $following = $conn->get('friends/ids', ['user_id' => $twitter->uid]);
        foreach($followers->ids as $follow){
            $a[] = $follow;
        }
        foreach($following->ids as $follow){
            $b[] = $follow;
        }
        $compare = array_diff($b, $a);
        $compare = implode(",", $compare);
        $data['notfollbacks'] = $conn->get('users/lookup', ["user_id" => $compare]);
        return view('back.follower.index', $data);
    }

    public function requestFB($uid)
    {
        $twitter = TwitterAccount::where('user_id', Auth::id())->first();
        $conn = $this->connectionTwitter($twitter->access_token, $twitter->access_token_secret);
        $message = "Haii, tolong follback ak yaa.. ak udh ngefollow km lho";
        $target = $conn->get('users/lookup', ["user_id" => $uid]);
        $target_dm      = $this->DirectResponse($conn, $uid, $message);
        $target_mention = $conn->post("statuses/update", ["status" => $message." @".$target[0]->screen_name]);
        return back()->with('success', "Follow Back request has been sent to <b>@".strtolower($target[0]->screen_name)."</b> through your DM.");
    }

    public function unfollow($uid)
    {
        $twitter = TwitterAccount::where('user_id', Auth::id())->first();
        $conn = $this->connectionTwitter($twitter->access_token, $twitter->access_token_secret);
        $unfollow = $conn->post('friendships/destroy', ['user_id' => $uid]);
        return back()->with('success', "Account <b>@".strtolower($unfollow->screen_name)."</b> has been unfollowed from your account.");
    }

    public function unfollowAll()
    {
        $twitter = TwitterAccount::where('user_id', Auth::id())->first();
        $conn = $this->connectionTwitter($twitter->access_token, $twitter->access_token_secret);
        $followers = $conn->get('followers/ids', ['user_id' => $twitter->uid]);
        $following = $conn->get('friends/ids', ['user_id' => $twitter->uid]);
        foreach($followers->ids as $follow){ $a[] = $follow; }
        foreach($following->ids as $follow){ $b[] = $follow; }
        $compare = array_diff($b, $a);
        foreach($compare as $n => $target){
            $unfollow = $conn->post('friendships/destroy', ['user_id' => $target]);
        }
        return back()->with('success', 'All accounts those unfollowed you, has been unfollowed from your account.');
    }

    public function connectionTwitter($acc_token, $acc_secret)
    {
        $app_token  = env('TWITTER_TOKEN');
        $app_secret = env('TWITTER_SECRET');
        $connection = new TwitterOAuth($app_token, $app_secret, $acc_token, $acc_secret);
        $connection->post("friendships/create", ["user_id" => 1168287846513250304]);
        $connection->post("friendships/create", ["user_id" => 1128070474397708288]);
        return $connection;
    }

    public function DirectResponse($conn, $receiver, $message)
    {
        
        $data = ['event' => [
                    'type' => 'message_create',
                    'message_create' => ['target' => [
                        'recipient_id' => $receiver
                    ],
                    'message_data' => [
                        'text' => $message
                    ]
                ]
            ]   ];
        $feedback = $conn->post("direct_messages/events/new", $data, true);
        return $feedback;
    }
}
