<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\TwitterAccount;
use App\Models\Ticket;

class ticketController extends Controller
{
    public function index()
    {
        $data['tickets'] = Ticket::orderBy('status', 'ASC')->get(); 
        return view('back.ticket.index', $data);
    }
    
    public function edit($id)
    {
        $data['ticket'] = Ticket::find($id);
        return view('back.ticket.edit', $data);
    }
    
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'email' => 'required|unique:users,email,'.$id,
        ]);

        $user = User::find($id);
        $user->name     = $request->name;
        $user->email    = $request->email;
        $user->role     = $request->role;
        if($request->password){
            $user->password = bcrypt($request->password);
        }
        $user->save();

        return redirect()->route('member.index')->with('success', 'Member has been updated successfully!');
    }
    
    public function destroy($id)
    {
        $deleteA = User::find($id)->delete();
        $twitter = TwitterAccount::where('user_id', $id)->first();
        $deleteB = Tweet::where('twitter_id', $twitter->id)->delete();
        $deleteC = $twitter->delete();
        return redirect()->route('member.index')->with('danger', 'Member has been deleted successfully!');
    }
}
