<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\TwitterAccount;
use Auth;

class MainController extends Controller
{
    public function index()
    {
        return view('back.home');
    }

    public function settings()
    {
        $data['account'] = User::find(Auth::id());
        return view('back.settings', $data);
    }

    public function updateSettings(Request $request)
    {
        $validatedData = $request->validate([
            'email' => 'required|unique:users,email,'.Auth::id(),
        ]);
        $account = User::find(Auth::id());
        $account->name = $request->name;
        $account->email = $request->email;
        if($request->password){
            $account->password = bcrypt($request->password);
        }
        $account->save();
        return redirect()->route('dashboard')->with('success', 'Your account configuration has been modified.');
    }
}
