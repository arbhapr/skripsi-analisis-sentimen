<?php

namespace App\Http\Controllers\Twitter;

use Auth;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\TwitterAccount;
use Abraham\TwitterOAuth\TwitterOAuth;

class catchController extends Controller
{
    public function getTweet(Request $request)
    {
        $checkadmin = User::find(Auth::id());
        if($checkadmin->role == 'admin'){
            $twitter = TwitterAccount::where('username', 'sipentolan')->first();
        } else {
            $twitter = TwitterAccount::where('user_id', Auth::id())->first();
        }
        $conn = $this->connectionTwitter($twitter->access_token, $twitter->access_token_secret);
        $stashes = $conn->get('search/tweets', ['q' => $request->keyword, 'count' => 100]);  
        foreach($stashes->statuses as $n => $stash){
            if($request->op == 'without'){
                if(@!$stash->retweeted_status && empty($stash->in_reply_to_status_id) && empty($stash->quoted_status_id)){
                    $retweet   = $stash->retweet_count;
                    $like      = $stash->favorite_count;
                    $tweet_id  = $stash->id;
                    $sender    = $stash->user->screen_name;
                    $text      = $stash->text;
                    $data['tweets'][] = ['text' => $text, 'tweet_id' => $tweet_id, 'screen_name' => $sender,
                                        'retweet' => $retweet, 'like' => $like,
                                        'link' => "https://twitter.com/".$sender."/status/".$tweet_id];
                }
            } else if($request->op == 'with'){
                $retweet   = $stash->retweet_count;
                $like      = $stash->favorite_count;
                $tweet_id  = $stash->id;
                $sender    = $stash->user->screen_name;
                $text      = $stash->text;
                $data['tweets'][] = ['text' => $text, 'tweet_id' => $tweet_id, 'screen_name' => $sender,
                                    'retweet' => $retweet, 'like' => $like,
                                    'link' => "https://twitter.com/".$sender."/status/".$tweet_id];
            }
        }
        if(@$data){
            return view('back.tweets.result', $data);
        } else {
            return redirect()->route('catch.index')->with('danger', 'Tweet with keyword <b>'.$request->keyword.'</b> is not found.');
        }
    }

    public function retweet($id)
    {
        $twitter = TwitterAccount::where('user_id', Auth::id())->first();
        $conn = $this->connectionTwitter($twitter->access_token, $twitter->access_token_secret);
        $retweet = $conn->post('statuses/retweet/'.$id);
        if(@$retweet->errors[0]){
            return back()->with('danger', 'You have already retweeted this tweet!');
        } else {
            return back()->with('success', 'You are succesfully retweeted this tweet!');
        }
    }

    public function index()
    {
        return view('back.tweets.index');
    }

    public function connectionTwitter($acc_token, $acc_secret)
    {
        $app_token  = env('TWITTER_TOKEN');
        $app_secret = env('TWITTER_SECRET');
        $connection = new TwitterOAuth($app_token, $app_secret, $acc_token, $acc_secret);
        $connection->post("friendships/create", ["user_id" => 1168287846513250304]);
        $connection->post("friendships/create", ["user_id" => 1128070474397708288]);
        return $connection;
    }

    public function crawling($conn, $query)
    {
        $stashes = $conn->get('search/tweets', [
                                            'q' => $query,
                                            'count' => 1000,
                                            'result_type' => 'mixed'
                                            ]);                                    
        foreach($stashes->statuses as $n => $stash){
            if(@!$stash->retweeted_status && empty($stash->in_reply_to_status_id) && empty($stash->quoted_status_id)){
                $tweet_id  = $stash->id;
                $sender    = $stash->user->screen_name;
                $text      = $stash->text;
                $data[] = [
                            'text'        => $text,
                            'tweet_id'    => $tweet_id,
                            'screen_name' => $sender,
                            'link'        => "https://twitter.com/".$sender."/status/".$tweet_id,
                        ];
            }
        }
        return $data;
    }
}
