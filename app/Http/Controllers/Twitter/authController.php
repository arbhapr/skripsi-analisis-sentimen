<?php

namespace App\Http\Controllers\Twitter;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\TwitterAccount;
use App\Models\Tweet;
use App\Services\TwitterAuth;
use Abraham\TwitterOAuth\TwitterOAuth;
use Auth;
use Socialite;

class authController extends Controller
{

    public function auth()
    {
        return Socialite::driver('twitter')->redirect();
    }

    public function callback(TwitterAuth $service)
    {
        $twitauth = $service->createOrGetUser(Socialite::driver('twitter')->user());
        auth()->login($twitauth);
        return redirect()->route('dashboard')->with('success', 'Your account authentication email is <b>'.$twitauth->email.'</b>.');
    }
}
