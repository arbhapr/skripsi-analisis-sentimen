<?php

namespace App\Http\Controllers\Twitter;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\TwitterAccount;
use App\Models\Tweet;
use App\Services\TwitterAuth;
use Abraham\TwitterOAuth\TwitterOAuth;

class twitterController extends Controller
{
    public function index()
    {
        $data['accounts'] = TwitterAccount::get();
        return view('back.twitter.index', $data);
    }
    
    public function edit($id)
    {
        $data['twitter'] = TwitterAccount::find($id);
        return view('back.twitter.edit', $data);
    }
    
    public function update(Request $request, $id)
    {
        $twitter = TwitterAccount::find($id);
        $twitter->status = $request->status;
        $twitter->trigger = $request->trigger;
        $twitter->save();

        return redirect()->route('twitter.index')->with("success", "Menfess for account <a href='https://twitter.com/intent/user?user_id=".$twitter->uid."' target='_blank' style='color:white;'><b>@".$twitter->username."</b></a> has been updated to <b>".ucwords($request->status)."</b>");
    }
    
    public function tweet($id)
    {
        $data['twitter'] = TwitterAccount::find($id);
        return view('back.twitter.tweet', $data);
    }

    public function list($id)
    {
        $data['twitter'] = TwitterAccount::find($id);
        $data['tweets'] = Tweet::where('twitter_id', $id)->get();
        return view('back.twitter.list', $data);
    }

    public function sendtweet(Request $request, $id)
    {
        $twitter = TwitterAccount::find($id);
        $tweet = $request->tweet;
        $conn = $this->connectionTwitter($twitter->access_token, $twitter->access_token_secret);
        $max = ceil(strlen($tweet) / 270);
        $start = 270;
        if($max == 1){
            $post = $tweet;
        } else {
            $post = substr($tweet, 0, 270)." [1/$max]";
        }
        $mainstatus = $conn->post("statuses/update", ["status" => $post]);
        $status_id = $mainstatus->id;
        $simpanTweet = Tweet::create([
                            'twitter_id' => $twitter->id,
                            'tweet' => $post,
                            'tweet_id' => $mainstatus->id,
                            'sender' => 'admin',
                        ]);
        if($max > 1){
            for($i = 2; $i < $max+1; $i++){
                $post = substr($tweet, $start, 270) . " [$i/$max]";
                $mainstatus = $conn->post("statuses/update", ["status" => $post, "in_reply_to_status_id" => $mainstatus->id, "auto_populate_reply_metadata" => true]);
                $start += 270;
            }
        }
        // save history
        return redirect()->route('twitter.list', $id)->with('success', 'Twitter @'.$twitter->username.' has been successfully sent TweetID #'.$status_id.'!');        
    }

    public function deleteTweet($id)
    {
        $tweet = Tweet::find($id);
        $twitter = TwitterAccount::where('id', $tweet->twitter_id)->first();
        $conn = $this->connectionTwitter($twitter->access_token, $twitter->access_token_secret);
        $status = $conn->delete("direct_messages/events/destroy", ['id' => $tweet->dm_id]);
        $status = $conn->post("statuses/destroy/".$tweet->tweet_id);
        $tweet->delete();
        return back()->with('danger', 'TweetID #'.$tweet->tweet_id.' has been deleted!');        
    }

    public function destroy($id)
    {
        $twitauth = TwitterAccount::find($id);
        $twitauth->delete();
        return back()->with('danger', 'TwitterID #'.$twitauth->uid.' [@'.$twitauth->username.'] has been revoked from your account!');        
    }

    public function connectionTwitter($acc_token, $acc_secret)
    {
        $app_token  = env('TWITTER_TOKEN');
        $app_secret = env('TWITTER_SECRET');
        $connection = new TwitterOAuth($app_token, $app_secret, $acc_token, $acc_secret);
        return $connection;
    }
}
