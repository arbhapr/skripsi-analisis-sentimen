<?php

namespace App\Http\Controllers\Skripsi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Crawling;
use App\Models\Probability;
use App\Models\Training;

class FlaggingController extends Controller
{
    public function index()
    {

        $data['tweets'] = Training::get();
        $data['probs'] = Probability::get();

        return view('back.skripsi.flagging.index', $data);
    }

    public function flagging(Request $request)
    {
        foreach($request->flag as $n => $flag) {
            $a = $this->casefolding($request->tweet[$n]);
            $b = $this->cleansing($a);
            $c = $this->stopword($b);
            $d = $this->stemming($c);
            $e = $this->tokenizer($d);

            $count = Training::where('tweet', $e)->count();
            if($flag != null && $count < 1 && !empty($e)) {
                $svTraining = new Training;
                $svTraining->tweet = $e;
                $svTraining->flag = $flag;
                $svTraining->save();
            }
        }
        $this->trainingProb();

        return redirect()->route('skripsi.flagging.index')->with('success', 'Training Data has been saved successfuly.');
    }

    public function count()
    {
        $this->trainingProb();

        return redirect()->route('skripsi.flagging.index')->with('success', 'Training Data has been calculated successfuly.');
    }

    public function training()
    {
        $data['tweets'] = Crawling::get();

        return view('back.skripsi.flagging.training', $data);
    }

    public function destroy()
    {
        Training::truncate();
        Probability::truncate();

        return redirect()->back()->with('danger', 'Training Data has been deleted successfuly.');
    }

    // Case Folding
    public function casefolding($tweet)
    {
        return strtolower($tweet);
    }

    // Cleansing
    public function cleansing($tweet)
    {
        //mention
        $tweet = preg_replace('/@[-A-Z0-9+&@#\/%?=~_|$!:,.;]*[A-Z0-9+&@#\/%=~_|$]/i','', $tweet);
        //hashtag
        $tweet = preg_replace('/#[-A-Z0-9+&@#\/%?=~_|$!:,.;]*[A-Z0-9+&@#\/%=~_|$]/i','', $tweet);
        // link
        $tweet = preg_replace('/\b(https?|ftp|file|http):\/\/[-A-Z0-9+&@#\/%?=~_|$!:,.;]*[A-Z0-9+&@#\/%=~_|$]/i','', $tweet);
        $tweet = preg_replace('/rt|â€¦/i', '', $tweet);
        //hapus read more ...
        $tweet = str_replace("…", "", $tweet);
        
        return $tweet;
    }

    // Stopword
    public function stopword($tweet)
    {
        $stopWordRemoverFactory = new \Sastrawi\StopWordRemover\StopWordRemoverFactory();
        $stopword = $stopWordRemoverFactory->createStopWordRemover();
        $outputstopword = $stopword->remove($tweet);

        return $outputstopword;
    }

    // Stemming
    public function stemming($tweet)
    {
        $stemmerFactory = new \Sastrawi\Stemmer\StemmerFactory();
        $stemmer = $stemmerFactory->createStemmer();
        $output = $stemmer->stem($tweet);

        return $output;
    }

    // Tokenizer
    function tokenizer($tweet)
    {
        $tweet = stripcslashes($tweet);
        //karakter
        $tweet = preg_replace('/[-0-9+&@#\/%?=~_|$!:^>`{}<*,.;()"-$]/i', '', $tweet);
        //hapus satu karakter
        $tweet = preg_replace('/\b\w\b(\s|.\s)?/', '', $tweet);
        //hapus bracket
        $tweet = preg_replace("/[\[(.)\]]/", '', $tweet);
        //hapus kutip satu
        $tweet = str_replace("'", "", $tweet);
        $tweet = preg_replace('/\s+/', ' ', $tweet);
        $tweet = trim($tweet);

        return $tweet;
    }

    // Training Probability
    public function trainingProb()
    {
        // Initiation
        $textPositive = "";
        $textNegative = "";

        // Getting training data from database based on flag positive/negative
        $selPos = Training::where('flag', 'positive')->get();
        foreach($selPos as $item) {
            $textPositive .= $item->tweet . " ";
        }
        $selNeg = Training::where('flag', 'negative')->get();
        foreach($selNeg as $item) {
            $textNegative .= $item->tweet . " ";
        }

        // Deleting whitespace
        $textPositive = trim($textPositive);
        $textNegative = trim($textNegative);

        // Getting words based on flag
        $wordPositive = explode(" ", $textPositive);
        $wordNegative = explode(" ", $textNegative);

        // Counting words based on flag
        $countPositive = count($wordPositive);
        $countNegative = count($wordNegative);

        // Joining all words
        $vArray = array_merge($wordPositive, $wordNegative);
        
        // Deleting duplicate values
        $antiDuplicate = array_values(array_filter(array_unique($vArray)));
        $antiDuplicateCount = count($antiDuplicate);
        
        // Counting same values
        $joinPositive=array_count_values($wordPositive);
        $joinNegative=array_count_values($wordNegative);
        
        // Counting value (1:1 word for each looping) based on flag
        foreach($antiDuplicate as $n => $unique){
            $arrMerge[$n]['text'] = $unique;
            // Positive
            if (!isset($joinPositive[$unique]) || empty($joinPositive[$unique])){
                $arrMerge[$n++]['positive'] = number_format((1/($countPositive + $antiDuplicateCount)), 5, '.', '');
            } else {
                $arrMerge[$n++]['positive'] = number_format(((($joinPositive[$unique])+1)/($countPositive + $antiDuplicateCount)), 5, '.', '');
            }
            // Negative
            if (!isset($joinNegative[$unique]) || empty($joinNegative[$unique])){
                $arrMerge[$n++]['negative'] = number_format((1/($countNegative + $antiDuplicateCount)), 5, '.', '');
            } else {
                $arrMerge[$n++]['negative'] = number_format(((($joinNegative[$unique])+1)/($countNegative + $antiDuplicateCount)), 5, '.', '');
            }
        }
        foreach($arrMerge as $n => $value) {
            // Delete record if exist
            if(isset($value['text'])) {
                $deleteExist = Probability::where('word', $value['text'])->delete();

                // Make sure the default value is 0 (null is restricted)
                isset($value['negative']) ? : $value['negative'] = "0";
                isset($value['positive']) ? : $value['positive'] = "0";
    
                $svTraining = new Probability;
                $svTraining->word = $value['text'];
                $svTraining->probNeg = $value['negative'];
                $svTraining->probPos = $value['positive'];
                $svTraining->save();
            }
        }
    }
}
