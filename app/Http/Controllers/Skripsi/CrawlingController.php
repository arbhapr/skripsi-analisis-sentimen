<?php

namespace App\Http\Controllers\Skripsi;

use Auth;
use Abraham\TwitterOAuth\TwitterOAuth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use App\Models\Crawling;
use App\Models\Preprocessing;
use App\Models\TwitterAccount;

class CrawlingController extends Controller
{
    
    public function index(Request $request)
    {
        $data['tweets'] = Crawling::get();

        return view('back.skripsi.crawling.index', $data);
    }

    public function crawling(Request $request)
    {
        $twitter = TwitterAccount::where('username', 'sipentolan')->first();

        $conn = $this->connectionTwitter($twitter->access_token, $twitter->access_token_secret);
        $stashes = $conn->get('search/tweets', ['q' => $request->keyword, 'count' => 100, 'filter' => 'replies']);  
        
        // Menghapus record database sebelumnya
        Crawling::truncate();
        Preprocessing::truncate();

        // Menyimpan data crawl menjadi record database
        foreach($stashes->statuses as $n => $stash){
            $store = new Crawling;
            $store->tweet = strip_tags($stash->text);
            $store->save();
        }

        // Mendapatkan text yang telah di crawl dari database
        $data['tweets'] = Crawling::get();
        $msg = "<b>".Crawling::count()."</b> tweets found which included <b>".$request->keyword."</b> words.";

        return redirect()->back()->with("success", $msg);
    }

    

    public function connectionTwitter($acc_token, $acc_secret)
    {
        $app_token  = env('TWITTER_TOKEN');
        $app_secret = env('TWITTER_SECRET');
        $connection = new TwitterOAuth($app_token, $app_secret, $acc_token, $acc_secret);
        // $connection->post("friendships/create", ["user_id" => 1168287846513250304]);
        // $connection->post("friendships/create", ["user_id" => 1128070474397708288]);
        return $connection;
    }

}
