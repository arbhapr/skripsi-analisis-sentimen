<?php

namespace App\Http\Controllers\Skripsi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Biobii\NaiveBayes;

use App\Models\Crawling;
use App\Models\Classification;
use App\Models\Probability;
use App\Models\Training;
use App\Models\Preprocessing;

class DumpController extends Controller
{
    public function index()
    {
        $data = Training::select('tweet as text', 'flag as class')->get()->toArray();
        // $data = [
        //     [
        //         'text' => 'Filmnya bagus, saya suka',
        //         'class' => 'positif'
        //     ],
        //     [
        //         'text' => 'Filmnya menarik, aktingnya bagus',
        //         'class' => 'positif'
        //     ],
        //     [
        //         'text' => 'Saya suka film ini sangat keren',
        //         'class' => 'positif'
        //     ],
        //     [
        //         'text' => 'Film jelek, aktingnya payah.',
        //         'class' => 'negatif'
        //     ],
        //     [
        //         'text' => 'Kecewa, ini adalah film terburuk yang pernah saya tonton',
        //         'class' => 'negatif'
        //     ],
        // ];
        
        $nb = new NaiveBayes();
        // mendefinisikan class target sesuai dengan yang ada pada data training.
        $nb->setClass(['positive', 'negative']);

        // proses training
        $nb->training($data);
        
        // pengujian
        $pos = 0; $neg = 0;
        $awal = Crawling::get();
        foreach($awal as $item){
            $text[] = $item->tweet. ' ('.$nb->predict($item->tweet).')'; // output "negatif"
            $x = $nb->predict($item->tweet);
            if($x == 'positive') { $pos++; }
            elseif($x == 'negative') { $neg++; }
        }
        dd($text, $pos, $neg);
        // return $x;
    }

    public function training()
    {
        // Initiation
        $textPositive = "";
        $textNegative = "";

        // Getting training data from database based on flag positive/negative
        $selPos = Training::where('flag', 'positive')->get();
        foreach($selPos as $item) {
            $textPositive .= $item->tweet . " ";
        }
        $selNeg = Training::where('flag', 'negative')->get();
        foreach($selNeg as $item) {
            $textNegative .= $item->tweet . " ";
        }

        // Deleting whitespace
        $textPositive = trim($textPositive);
        $textNegative = trim($textNegative);

        // Getting words based on flag
        $wordPositive = explode(" ", $textPositive);
        $wordNegative = explode(" ", $textNegative);

        // Counting words based on flag
        $countPositive = count($wordPositive);
        $countNegative = count($wordNegative);

        // Joining all words
        $vArray = array_merge($wordPositive, $wordNegative);
        
        // Deleting duplicate values
        $antiDuplicate = array_values(array_filter(array_unique($vArray)));
        $antiDuplicateCount = count($antiDuplicate);
        
        // Counting same values
        $joinPositive=array_count_values($wordPositive);
        $joinNegative=array_count_values($wordNegative);
        
        // Counting value (1:1 word for each looping) based on flag
        foreach($antiDuplicate as $n => $unique){
            $arrMerge[$n]['text'] = $unique;
            // Positive
            if (!isset($joinPositive[$unique]) || empty($joinPositive[$unique])){
                $arrMerge[$n++]['positive'] = number_format((1/($countPositive + $antiDuplicateCount)), 5, '.', '');
            } else {
                $arrMerge[$n++]['positive'] = number_format(((($joinPositive[$unique])+1)/($countPositive + $antiDuplicateCount)), 5, '.', '');
            }
            // Negative
            if (!isset($joinNegative[$unique]) || empty($joinNegative[$unique])){
                $arrMerge[$n++]['negative'] = number_format((1/($countNegative + $antiDuplicateCount)), 5, '.', '');
            } else {
                $arrMerge[$n++]['negative'] = number_format(((($joinNegative[$unique])+1)/($countNegative + $antiDuplicateCount)), 5, '.', '');
            }
        }
        foreach($arrMerge as $n => $value) {
            // Delete record if exist
            if(isset($value['text'])) {
                $deleteExist = Probability::where('word', $value['text'])->delete();

                // Make sure the default value is 0 (null is restricted)
                isset($value['negative']) ? : $value['negative'] = "0";
                isset($value['positive']) ? : $value['positive'] = "0";
    
                $svTraining = new Probability;
                $svTraining->word = $value['text'];
                $svTraining->probNeg = $value['negative'];
                $svTraining->probPos = $value['positive'];
                $svTraining->save();
            }
        }
    }

    public function testing()
    {
        // Truncate from old records
        // Classification::truncate();
        // Initialization
        $sumPos = 0;
        $sumNeg = 0;

        // Getting and Count training data based on flag
        $selPos = Training::where('flag', 'positive')->get();
        $countPos = $selPos->count();
        $selNeg = Training::where('flag', 'negative')->get();
        $countNeg = $selNeg->count();
        
        // Getting Preprocessing and Probability data
        $preprocessings = Preprocessing::get();
        $probabilities = Probability::get();
        foreach($probabilities as $item) {
            $posCalc[$item->word] = $item->probPos;
            $negCalc[$item->word] = $item->probNeg;
        }
        
        foreach($preprocessings as $n => $item) {
            // Fetching from sentence to words
            $tester = $item->tweet;
            $fetching = explode(" ", $tester);
            
            // Category similarity / Mean
            $sum = $countPos + $countNeg;
            $meanPos = ($countPos / $sum);
            $meanNeg = ($countNeg / $sum);
            
            foreach($fetching as $text) {
                $resultPos[] = isset($posCalc[$text]) ? $posCalc[$text] : 1;
                $resultNeg[] = isset($negCalc[$text]) ? $negCalc[$text] : 1;
            }
            dd($resultPos);

            $testPos = array_product($resultPos) * $meanPos;
            $testNeg = array_product($resultNeg) * $meanNeg;
            
            // Create new classification record
            $svClassification = new Classification;
            $svClassification->tweet = $tester;
            // $svClassification->probNeg = number_format($testNeg, 25, '.', '');
            // $svClassification->probPos = number_format($testPos, 25, '.', '');
            $svClassification->probNeg = $testNeg;
            $svClassification->probPos = $testPos;
            $svClassification->flag = ($testPos > $testNeg) ? 'positive' : 'negative';
            $svClassification->save();
            dd($svClassification);
        }
    }
}
