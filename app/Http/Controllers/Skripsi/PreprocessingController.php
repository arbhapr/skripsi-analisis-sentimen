<?php

namespace App\Http\Controllers\Skripsi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Crawling;
use App\Models\Preprocessing;

class PreprocessingController extends Controller
{
    public function index()
    {
        $tweets = Crawling::get();
        if($tweets->count() > 0){
            foreach($tweets as $n => $item) {
                $a = $this->casefolding($item->tweet);
                $b = $this->cleansing($a);
                $c = $this->stopword($b);
                $d = $this->stemming($c);
                $e = $this->tokenizer($d);

                $data['casefolding'][]  = $a;
                $data['cleansing'][]    = $b;
                $data['stopword'][]     = $c;
                $data['stemming'][]     = $d;
                $data['tokenization'][] = $e;
            }
        } else {
            $data['casefolding'] = null;
        }

        return view('back.skripsi.preprocessing.index', $data);
    }

    // Case Folding
    public function casefolding($tweet)
    {
        return strtolower($tweet);
    }

    // Cleansing
    public function cleansing($tweet)
    {
        //mention
        $tweet = preg_replace('/@[-A-Z0-9+&@#\/%?=~_|$!:,.;]*[A-Z0-9+&@#\/%=~_|$]/i','', $tweet);
        //hashtag
        $tweet = preg_replace('/#[-A-Z0-9+&@#\/%?=~_|$!:,.;]*[A-Z0-9+&@#\/%=~_|$]/i','', $tweet);
        // link
        $tweet = preg_replace('/\b(https?|ftp|file|http):\/\/[-A-Z0-9+&@#\/%?=~_|$!:,.;]*[A-Z0-9+&@#\/%=~_|$]/i','', $tweet);
        $tweet = preg_replace('/rt|â€¦/i', '', $tweet);
        //hapus read more ...
        $tweet = str_replace("…", "", $tweet);
        
        return $tweet;
    }

    // Stopword
    public function stopword($tweet)
    {
        $stopWordRemoverFactory = new \Sastrawi\StopWordRemover\StopWordRemoverFactory();
        $stopword = $stopWordRemoverFactory->createStopWordRemover();
        $outputstopword = $stopword->remove($tweet);

        return $outputstopword;
    }

    // Stemming
    public function stemming($tweet)
    {
        $stemmerFactory = new \Sastrawi\Stemmer\StemmerFactory();
        $stemmer = $stemmerFactory->createStemmer();
        $output = $stemmer->stem($tweet);

        return $output;
    }

    // Tokenizer
    function tokenizer($tweet)
    {
        $tweet = stripcslashes($tweet);
        //karakter
        $tweet = preg_replace('/[-0-9+&@#\/%?=~_|$!:^>`{}<*,.;()"-$]/i', '', $tweet);
        //hapus satu karakter
        $tweet = preg_replace('/\b\w\b(\s|.\s)?/', '', $tweet);
        //hapus bracket
        $tweet = preg_replace("/[\[(.)\]]/", '', $tweet);
        //hapus kutip satu
        $tweet = str_replace("'", "", $tweet);
        $tweet = preg_replace('/\s+/', ' ', $tweet);
        $tweet = trim($tweet);

        // Saving result of preprocessing text
        $count = Preprocessing::where('tweet', $tweet)->count();
        if($count < 1 && !empty($tweet)) {
            $svPreprocessing = new Preprocessing;
            $svPreprocessing->tweet = $tweet;
            $svPreprocessing->save();
        }

        return $tweet;
    }

    // vardump
    public function dump()
    {
        $text = "RT @GerryS: 14/22: Pemerintah mengharuskan airline² tetap merefund, sementara airline² masih harus bayar gaji karyawan dan sewa pesawat, ta…";
        // function
        $result = $this->cleansing($text);
        return $result;
    }
}
