<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\TwitterAccount;
use App\Models\Tweet;
use App\Models\Ticket;
use Abraham\TwitterOAuth\TwitterOAuth;

class apiController extends Controller
{

    public function menfess()
    {
        $twitters = TwitterAccount::where('status', 'active')->get();
        foreach($twitters as $twitter){
            $conn = $this->connectionTwitter($twitter->access_token, $twitter->access_token_secret);
            $dms = $conn->get("direct_messages/events/list");
            foreach($dms->events as $n => $dm){
                $message = $dm->message_create->message_data->text;
                $sender_id = $dm->message_create->sender_id;
                $dm_id = $dm->id;
                if(strpos($message, $twitter->trigger) !== false){
                    if(Tweet::where('tweet', $message)->where('twitter_id', $twitter->id)->count() < 1){
                        // media should be downloaded and reuploaded before tweeting any tweet
                        // Twitter Problem**
                        $max = ceil(strlen($message) / 270);
                        $start = 270;
                        if($max == 1){
                            $post = $message;
                        } else {
                            $post = substr($message, 0, 270)." [1/$max]";
                        }
                        $mainstatus = $conn->post("statuses/update", ["status" => $post]);
                        $simpanTweet = Tweet::create([
                                            'dm_id' => $dm_id,
                                            'twitter_id' => $twitter->id,
                                            'tweet' => $message,
                                            'tweet_id' => $mainstatus->id,
                                            'sender' => $sender_id,
                                        ]);
                        $text =  'https://twitter.com/'.$twitter->username.'/status/'.$mainstatus->id;                            
                        $data = $this->DirectResponse($conn, $sender_id, $text);

                        if($max > 1){
                            for($i = 2; $i < $max+1; $i++){
                                $post = substr($message, $start, 270) . " [$i/$max]";
                                $mainstatus = $conn->post("statuses/update", ["status" => $post, "in_reply_to_status_id" => $mainstatus->id, "auto_populate_reply_metadata" => true]);
                                $start += 270;
                            }
                        }
                    }
                }
            }
        }
        return 'program running successfully';
    }

    public function demo($id)
    {
        $twitter = TwitterAccount::find($id);
        $conn = $this->connectionTwitter($twitter->access_token, $twitter->access_token_secret);
        
        $media = "https://itemku-upload.s3.ap-southeast-1.amazonaws.com/20180703/5b3ad6258f7fa.jpg";
        $x = $this->retrieve($id, $media);
        $with_media = $conn->upload('media/upload', ['media' => $media, 'total_bytes' => $x, 'media_type' => 'jpg'], true);
        $parameters = [
            'status' => '[DEVELOPER-DEBUG] trying to post a tweet using media',
            'media_ids' => $with_media->media_id
        ];
        $result = $conn->post('statuses/update', $parameters);
        return $result;
    }

    public function retrieve($url, $id){
        $conn = $this->connectionTwitter($twitter->access_token, $twitter->access_token_secret);
        $twitter = TwitterAccount::find($id);        
        $ch = curl_init($url);
        $headerAuth = "Authorization: OAuth oauth_version='1.0',
                        oauth_consumer_key='{yourAppToken}',
                        oauth_token='{userToken}',
                        oauth_signature_method='HMAC-SHA1'";
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [$headerAuth]);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_NOBODY, TRUE);
   
        $data = curl_exec($ch);
        $size = curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD);
   
        curl_close($ch);
        return $size;
   }

    public function connectionTwitter($acc_token, $acc_secret)
    {
        $app_token = env('TWITTER_TOKEN');     
        $app_secret = env('TWITTER_SECRET');
        $connection = new TwitterOAuth($app_token, $app_secret, $acc_token, $acc_secret);
        return $connection;
    }

    public function DirectResponse($conn, $receiver, $message)
    {
        
        $data = ['event' => [
                    'type' => 'message_create',
                    'message_create' => ['target' => [
                        'recipient_id' => $receiver
                    ],
                    'message_data' => [
                        'text' => $message
                    ]
                ]
            ]   ];
        $feedback = $conn->post("direct_messages/events/new", $data, true);
        return $feedback;
    }
}
