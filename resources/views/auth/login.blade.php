<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>{{ config('app.name', 'AdminBSB - Boilerplate') }}</title>

    <link rel="icon" href="{{asset('vendor/tile')}}/img/favicon.png" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet"
        type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    <link href="{{asset('vendor/AdminBSB')}}/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link href="{{asset('vendor/AdminBSB')}}/plugins/node-waves/waves.css" rel="stylesheet" />
    <link href="{{asset('vendor/AdminBSB')}}/plugins/animate-css/animate.css" rel="stylesheet" />
    <link href="{{asset('vendor/AdminBSB')}}/css/style.css" rel="stylesheet">
</head>

<body class="login-page">
    <div class="login-box">
        <div class="logo">
            <a href="javascript:void(0);"><b>Login</b> Area</a>
        </div>
        <div class="card">
            <div class="body">
                <form method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="msg"><small>Login to access {{ config('app.name', 'AdminBSB - Boilerplate') }}.</small></div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                        <div class="form-line">
                            <input id="email" type="text"
                                class="form-control @error('email') is-invalid @enderror" name="email"
                                value="{{ old('email') }}" required placeholder="Email" autocomplete="email"
                                autofocus>
                        </div>
                        @error('email')
                        <small style="color:red;">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input id="password" type="password"
                                class="form-control @error('password') is-invalid @enderror" name="password" required
                                placeholder="Password" autocomplete="current-password">
                        </div>
                        @error('password')
                        <small style="color:red;">{{ $message }}</small>
                        @enderror
                    </div>
                    <div class="row">
                        <div class="col-md-6" style="margin-bottom: 0px;">
                            <button type="submit" class="btn btn-primary col-xs-12">Auth Login</button>
                        </div>
                        <div class="col-md-6">
                            <a class="btn btn-primary col-xs-12" href="{{route('twitter-auth')}}" style="color:#FFF;">Twitter Login</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script src="{{asset('vendor/AdminBSB')}}/plugins/jquery/jquery.min.js"></script>
    <script src="{{asset('vendor/AdminBSB')}}/plugins/bootstrap/js/bootstrap.js"></script>
    <script src="{{asset('vendor/AdminBSB')}}/plugins/node-waves/waves.js"></script>
    <script src="{{asset('vendor/AdminBSB')}}/plugins/jquery-validation/jquery.validate.js"></script>
    <script src="{{asset('vendor/AdminBSB')}}/js/admin.js"></script>
    <script src="{{asset('vendor/AdminBSB')}}/js/pages/examples/sign-in.js"></script>
</body>

</html>
