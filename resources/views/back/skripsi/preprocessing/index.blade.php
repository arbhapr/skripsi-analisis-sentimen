@extends('back.master')
@section('content')
<div class="row clearfix">
    <div class="col-md-12">
        <div class="card">
            <div class="header">
                <div class="row clearfix">
                    <div class="col-md-10">
                        <h2>Preprocessed Tweets</h2>
                    </div>
                </div>
            </div>
            
            <div class="body">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs tab-nav-right" role="tablist">
                    <li class="active"><a href="#casefolding" data-toggle="tab">Case Folding</a></li>
                    <li><a href="#cleansing" data-toggle="tab">Cleansing</a></li>
                    <li><a href="#sw_removal" data-toggle="tab">Stopword Removal</a></li>
                    <li><a href="#stemming" data-toggle="tab">Stemming</a></li>
                    <li><a href="#tokenization" data-toggle="tab">Tokenization</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="casefolding">
                        <div class="table-responsive">
                            <table class="table table-hover table-striped table-bordered js-basic-example dataTable">
                                <thead>
                                    <tr>
                                        <th width="5%">#</th>
                                        <th>Tweet</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(empty($casefolding))
                                    <tr>
                                        <td colspan="3">No data available in table</td>
                                    </tr>
                                    @else
                                    @foreach($casefolding as $n => $item)
                                    <tr>
                                        <td width="1%">{{ $n+1 }}</td>
                                        <td>{{ $item }}</td>
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="cleansing">
                        <div class="table-responsive">
                            <table class="table table-hover table-striped table-bordered js-basic-example dataTable">
                                <thead>
                                    <tr>
                                        <th width="5%">#</th>
                                        <th>Tweet</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(empty($casefolding))
                                    <tr>
                                        <td colspan="3">No data available in table</td>
                                    </tr>
                                    @else
                                    @foreach($cleansing as $n => $item)
                                    <tr>
                                        <td width="1%">{{ $n+1 }}</td>
                                        <td>{{ $item }}</td>
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="sw_removal">
                        <div class="table-responsive">
                            <table class="table table-hover table-striped table-bordered js-basic-example dataTable">
                                <thead>
                                    <tr>
                                        <th width="5%">#</th>
                                        <th>Tweet</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(empty($casefolding))
                                    <tr>
                                        <td colspan="3">No data available in table</td>
                                    </tr>
                                    @else
                                    @foreach($stopword as $n => $item)
                                    <tr>
                                        <td width="1%">{{ $n+1 }}</td>
                                        <td>{{ $item }}</td>
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="stemming">
                        <div class="table-responsive">
                            <table class="table table-hover table-striped table-bordered js-basic-example dataTable">
                                <thead>
                                    <tr>
                                        <th width="5%">#</th>
                                        <th>Tweet</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(empty($casefolding))
                                    <tr>
                                        <td colspan="3">No data available in table</td>
                                    </tr>
                                    @else
                                    @foreach($stemming as $n => $item)
                                    <tr>
                                        <td width="1%">{{ $n+1 }}</td>
                                        <td>{{ $item }}</td>
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="tokenization">
                        <div class="table-responsive">
                            <table class="table table-hover table-striped table-bordered js-basic-example dataTable">
                                <thead>
                                    <tr>
                                        <th width="5%">#</th>
                                        <th>Tweet</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(empty($casefolding))
                                    <tr>
                                        <td colspan="3">No data available in table</td>
                                    </tr>
                                    @else
                                    @foreach($tokenization as $n => $item)
                                    <tr>
                                        <td width="1%">{{ $n+1 }}</td>
                                        <td>{{ $item }}</td>
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection