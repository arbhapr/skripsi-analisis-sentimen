@extends('back.master')
@section('content')
<div class="row clearfix">
    <div class="col-md-12">
        <div class="card">
            <div class="header">
                <div class="row clearfix">
                    <div class="col-md-12">
                        <h2>Crawling Data</h2>
                    </div>
                </div>
            </div>
            <div class="body">
                <form action="{{route('skripsi.crawling.crawling')}}" method="POST" autocomplete="off">
                    @csrf
                    <div class="row clearfix">
                        <div class="col-md-10 col-sm-12">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" name="keyword" class="form-control" value="{{ old('keyword') }}" required />
                                    <label class="form-label">Search Keyword</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-12 text-right">
                            <button type="submit" class="btn btn-sm btn-primary waves-effect">
                                <i class="material-icons">search</i>
                                <span>SEARCH</span>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="card">
            <div class="header">
                <div class="row clearfix">
                    <div class="col-md-10">
                        <h2>Tweets List</h2>
                    </div>
                </div>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-hover table-striped table-bordered js-basic-example dataTable">
                        <thead>
                            <tr>
                                <th width="5%">#</th>
                                <th>Tweet</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($tweets->count() < 1)
                            <tr>
                                <td colspan="3">No data available in table</td>
                            </tr>
                            @else
                            @foreach($tweets as $n => $item)
                            <tr>
                                <td>{{ $n+1 }}</td>
                                <td>{{ $item->tweet }}</td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection