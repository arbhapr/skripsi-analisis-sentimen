@extends('back.master')
@section('content')
<div class="row clearfix">
    <div class="col-md-12">
        <div class="card">
            <div class="header">
                <div class="row clearfix">
                    <div class="col-md-10">
                        <h2>Training Module</h2>
                    </div>
                </div>
            </div>
            <div class="body">
                
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12 col-md-4">
                            <a href="{{route('skripsi.flagging.training')}}" type="button" class="btn btn-primary waves-effect m-t-20 m-b-20" style="width:100%;">
                                <span>ADD TRAINING DATA</span>
                            </a>
                        </div>
                        <div class="col-sm-12 col-md-4">
                            <a href="{{route('skripsi.flagging.count')}}" type="button" class="btn btn-warning waves-effect m-t-20 m-b-20" style="width:100%;">
                                <span>CALCULATE PROBABILITY</span>
                            </a>
                        </div>
                        <div class="col-sm-12 col-md-4">
                            <a href="{{route('skripsi.flagging.destroy')}}" type="button" class="btn btn-danger waves-effect m-t-20 m-b-20" style="width:100%;" onclick="return confirm('Are you sure want to clear current training data?')">
                                <span>CLEAR TRAINING DATA</span>
                            </a>
                        </div>
                    </div>
                </div>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs tab-nav-right" role="tablist">
                    <li class="active"><a href="#trainingData" data-toggle="tab">Training Data</a></li>
                    <li><a href="#trainingProb" data-toggle="tab">Training Probability</a></li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane fade in active" id="trainingData">
                        <div class="table-responsive">
                            <table class="table table-hover table-striped table-bordered js-basic-example dataTable">
                                <thead>
                                    <tr>
                                        <th width="5%">#</th>
                                        <th>Tweet</th>
                                        <th width="5%">Flag</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if($tweets->count() < 1)
                                    <tr>
                                        <td colspan="3">No data available in table</td>
                                    </tr>
                                    @else
                                    @foreach($tweets as $n => $item)
                                    <tr>
                                        <td>{{ $n+1 }}</td>
                                        <td>{{ $item->tweet }}</td>
                                        <td>{{ ucwords($item->flag) }}
                                        </td>
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="trainingProb">
                        <div class="table-responsive">
                            <table class="table table-hover table-striped table-bordered js-basic-example dataTable">
                                <thead>
                                    <tr>
                                        <th width="5%">#</th>
                                        <th>Word</th>
                                        <th width="5%">Positive Prob</th>
                                        <th width="5%">Negative Prob</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if($probs->count() < 1)
                                    <tr>
                                        <td colspan="3">No data available in table</td>
                                    </tr>
                                    @else
                                    @foreach($probs as $n => $item)
                                    <tr>
                                        <td>{{ $n+1 }}</td>
                                        <td>{{ $item->word }}</td>
                                        <td>{{ $item->probPos }}</td>
                                        <td>{{ $item->probNeg }}</td>
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection