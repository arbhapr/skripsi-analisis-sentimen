@extends('back.master')
@section('content')
<div class="row clearfix">
    <div class="col-md-12">
        <div class="card">
            <div class="header">
                <div class="row clearfix">
                    <div class="col-md-10">
                        <h2>Training Data</h2>
                    </div>
                </div>
            </div>
            <div class="body">
                <form method="POST" action="{{ route('skripsi.flagging.flagging') }}">
                    @csrf
                    <div class="table-responsive">
                        <table class="table table-hover table-striped table-bordered js-basic-example dataTable">
                            <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th>Tweet</th>
                                    <th width="5%">Mark As</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($tweets->count() < 1)
                                <tr>
                                    <td colspan="3">No data available in table</td>
                                </tr>
                                @else
                                @foreach($tweets as $n => $item)
                                <tr>
                                    <td>{{ $n+1 }}</td>
                                    <td>{{ $item->tweet }}</td>
                                    <td>
                                        <input type="hidden" name="tweet[]" value="{{ $item->tweet }}">
                                        <select name="flag[]" class="form-control">
                                            <option value="">Neutral</option>
                                            <option value="positive">Positive</option>
                                            <option value="negative">Negative</option>
                                        </select>
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                    </div>
                    <button type="submit" class="btn btn-primary waves-effect m-t-20 m-b-20" style="width:100%;">
                        <span>SUBMIT</span>
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection