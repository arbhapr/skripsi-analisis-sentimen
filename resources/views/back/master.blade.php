﻿@php
    $loguser = App\User::find(Auth::id());
    $reltwitter = App\Models\TwitterAccount::where('user_id', Auth::id())->count();
@endphp
<!DOCTYPE html>
<html>
    <head>
        @include('back.partials.head')
    </head>
    <body class="theme-blue">
        {{-- @include('back.partials.loader') --}}
        @include('back.partials.navbar')
        <section>
            @include('back.partials.left-sidebar')
        </section>
        <section class="content">
            <div class="container-fluid">
                @if(Session::get('success'))
                    <div class="alert alert-alert-dismissible bg-blue" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        {!! Session::get('success') !!}
                    </div>
                @elseif(Session::get('danger'))
                    <div class="alert alert-alert-dismissible alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        {!! Session::get('danger') !!}
                    </div>
                @endif
                @yield('content')
            </div>
        </section>
        @include('back.partials.script')
        @yield('script')
    </body>
</html>
