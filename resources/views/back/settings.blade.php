@extends('back.master')
@section('content')
<div class="row clearfix">
    <div class="col-md-12">
        <div class="card">
            <div class="header">
                <div class="row clearfix">
                    <div class="col-md-12">
                        <h2>Account Configuration</h2>
                    </div>
                </div>
            </div>
            <div class="body">
                <form action="{{route('updateSettings')}}" method="POST" autocomplete="off">
                    @csrf
                    <div class="row clearfix">
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="text" name="name" class="form-control" value="{{$account->name}}" required />
                                    <label class="form-label">Name</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="email" name="email" class="form-control" value="{{$account->email}}" required />
                                    <label class="form-label">E-Mail</label>
                                </div>
                                @error('email')
                                <small style="color:red;">{{ $message }}</small>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group form-float">
                                <div class="form-line">
                                    <input type="password" name="password" class="form-control"/>
                                    <label class="form-label">Password {{empty($account->password)?'[fill it for Auth Login]':'[optional]'}}</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <input type="submit" value="Submit" class="btn btn-primary btn-lg">
                            <input type="reset" value="Reset" class="btn btn-warning btn-lg">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<a type="button" class="btn btn-warning btn-circle-lg waves-effect waves-circle waves-float"
    style="position:fixed; bottom:20px; right:20px;" href="{{route('dashboard')}}">
    <i class="material-icons">undo</i>
</a>
@endsection