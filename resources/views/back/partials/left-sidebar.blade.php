<aside id="leftsidebar" class="sidebar">
    <div class="user-info">
        <div class="row">
            <a href="{{route('settings')}}">
                <div class="image col-xs-2">
                    @if(empty($loguser->avatar))
                        <img src="{{asset('vendor/AdminBSB')}}/images/user.png" width="48" height="48" alt="User" />
                    @else
                        <img src="{{$loguser->avatar}}" width="48" height="48" alt="User" />
                    @endif
                </div>
            </a>
            <div class="info-container col-xs-7">
                <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <b>{{$loguser->name}}</b></div>
                <div class="email">{{$loguser->email}}</div>
            </div>
            <div class="info-container col-xs-2" style="top:20px;">
                <a href="{{ route('logout') }}" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();" style="color:white;" title="Keluar">
                    <i class="material-icons">exit_to_app</i>
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
        </div>
    </div>
    
    <div class="menu">
        <ul class="list">
            <li class="header">MAIN NAVIGATION</li>
            <li class="{{(Request::segment(1) == 'dashboard')?'active':''}}">
                <a href="{{route('dashboard')}}">
                    <i class="material-icons">home</i>
                    <span>Home</span>
                </a>
            </li>

{{-- Tools Naive-Bayes --}}
            <li class="header">NAIVE-BAYES</li>
            <li class="{{(Request::segment(1) == 'crawling')?'active':''}}">
                <a href="{{route('skripsi.crawling.index')}}">
                    <i class="material-icons">search</i>
                    <span>Crawl Tweets</span>
                </a>
            </li>
            <li class="{{(Request::segment(1) == 'flagging')?'active':''}}">
                <a href="{{route('skripsi.flagging.index')}}">
                    <i class="material-icons">flag</i>
                    <span>Training</span>
                </a>
            </li>
            <li class="{{(Request::segment(1) == 'preprocessing')?'active':''}}">
                <a href="{{route('skripsi.preprocessing.index')}}">
                    <i class="material-icons">settings</i>
                    <span>Preprocessing</span>
                </a>
            </li>
{{-- Tools Naive-Bayes --}}

        </ul>
    </div>
    
    <div class="legal">
        <div class="copyright">
            &copy; {{date('Y')}} <a href="javascript:void(0);">{{ config('app.name', 'AdminBSB - Boilerplate') }}</a>.
        </div>
        <div class="version">
            <b>Version: </b> {{ env('APP_VERSION', 'Developing') }}
        </div>
    </div>
</aside>